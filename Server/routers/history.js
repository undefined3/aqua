const express = require('express');
const router = express.Router();
// const week = require('../History');
// const month =require('../History');

const week = [
    { "day": "Mon", "usage": 2.65 },
    { "day": "Tue", "usage": 2.45 },
    { "day": "Wed", "usage": 3.5 },
    { "day": "Thu", "usage": 2.2 },
    { "day": "Fri", "usage": 2.15 },
    { "day": "Sat", "usage": 3.65 },
    { "day": "Sun", "usage": 2.8 }
];  

const month = [
    { "month": "Jan", "usage": 56.1 },
    { "month": "Feb", "usage": 62.2 },
    { "month": "Mar", "usage": 56.6 },
    { "month": "Apr", "usage": 59.32 },
    { "month": "May", "usage": 55.3 },
    { "month": "Jun", "usage": 60.4 },
    { "month": "Jul", "usage": 65.4 },
    { "month": "Aug", "usage": 61.2 },
    { "month": "Sep", "usage": 54.7 },
    { "month": "Oct", "usage": 55.45 },
    { "month": "Nov", "usage": 53.52 },
    { "month": "Dec", "usage": 54.7 }
];



// get week data
router.get('/week', (req, res) => {
    
    res.json(week);
});

// get single day data
router.get('/week/:day', (req,res) => {
    const found = week.some(week => week.day === req.params.day);
    
    if(found){
        res.json(week.filter(week => week.day === req.params.day));
    }
    else{
        res.status(400).json({ msg: 'Invalid day' });
    }
});

// get month data
router.get('/month', (req, res) => {
    res.json(month);
});

// update day data
router.put('/week/:day', (req, res) => {
    const found = week.some(week => week.day ===req.params);

    if(found){
        const updDay = req.body;
        week.forEach(week => {
            if(week.day === updDay.day){
                week.usage = updDay.usage? updDay.usage : week.usage;
            }
        });
    }
    else{
        res.status(400).json({ msg: 'Invalid day' });
    }

});



module.exports = router;