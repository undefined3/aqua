import 'dart:async';
import 'dart:convert';

import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/shedulService.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';

class DashboardService {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  CommoneVar _commoneVar = new CommoneVar();
  UserService _userService = new UserService();

  Future getUserData() async {
    final SharedPreferences prefs = await _prefs;

    String token = prefs.getString('jwt');
    Map<String, String> headers = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization': 'Barear $token'
    };

    // String url = _commoneVar.getUrl();

    String url = "Hello";
    return url;
  }

  Future<Map> getSummary() async{
    
    UserService userService=new UserService();
    Map user=await userService.decodeData();
    String url = "${_commoneVar.getUrl()}dashboard/summary/${user['id']}";
    print(url);
    Response result = await get(url);

    print("\n\n\n\n\n\\n\n");  
    print(jsonDecode(result.body));  
    if(jsonDecode(result.body)['status']){
      print("hi");
    }else{
      SheduleService sheduleService=new SheduleService();
      await sheduleService.getShedule();
      return await this.getSummary();
      
    }
    return jsonDecode(result.body)['data'];
  }

  Future<Map> addWater(var volume, bool status) async {
    final SharedPreferences prefs = await _prefs;

    String userId = "5f00081047b83917c024e67d";

    var dataObject = {
      "userId": userId,
      "volume": volume.toString(),
      "toAdd": status.toString()
    };

    String token = prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Barear $token'
    };

    String url = "${_commoneVar.getUrl()}dashboard/addWater";
    print(url);
    Response result =
        await post(url, headers: requestHeaders, body: dataObject);

    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  Future<Map> addBarcode(value) async {
    final SharedPreferences prefs = await _prefs;

    String userId = "5f00081047b83917c024e67d";

    var dataObject = {"userId": userId, "barcode": value};

    String token = prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization': 'Barear $token'
    };

    String url = "${_commoneVar.getUrl()}user/barcode";
    print(url);
    Response result =
        await post(url, headers: requestHeaders, body: dataObject);

    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }
}
