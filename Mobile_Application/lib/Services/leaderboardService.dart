import 'dart:io';

import 'package:http/http.dart';
import 'dart:convert';
import 'comonvar.dart';
import 'package:shared_preferences/shared_preferences.dart';



class LeaderBoardService{
  CommoneVar _comnvar=CommoneVar();
  List friend=[];
  LeaderBoardService();

 Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

// Set group id 
  

// Get all members of group given type
// type can be weekly, daily, monthly 
  Future<Map> getFriend(String marktype) async {
    final SharedPreferences prefs = await _prefs;

    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/$marktype";
    print(url);
    Response result=await get(url,headers: requestHeaders);
    
    
    print(jsonDecode(result.body));
    return jsonDecode(result.body);

  }

// Call above function
  // Future<> (String marktype) async{

  //   await this.setFriend(marktype);
  // }

  // Get all groups list 
  Future<List> getGroup() async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/getall";
    Response result=await get(url,headers:requestHeaders);
    jsonDecode(result.body);
    
    List groups=jsonDecode(result.body);
    print("groups.length");
    print(groups.length);
    for(var i=0;i<groups.length;i++){
      groups[i]['spinner']=0;
    }
    if(groups.length==0){
      return new List();
    }else{
      return groups;
    }
    // return [];
  }

  // Send request to given group 
  Future<bool> sendGroupRequest(id) async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token' 
    };
    String url="${_comnvar.getUrl()}leaderBoard/addmember/$id";
    Response result=await get(url,headers:requestHeaders);
    jsonDecode(result.body);
    // List groups=jsonDecode(result.body);
    print("hell");
    print(jsonDecode(result.body)['status']);
    await prefs.setString('jwt',jsonDecode(result.body)['token']);
    return jsonDecode(result.body)['status'];
    // return [];
  }

  // Create new group
  Future<Map> creatGroup(File image,String name) async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/addgroup";
    String base64Image = base64Encode(image.readAsBytesSync());
    String fileExtension = (image.path.split("/").last).split('.').last;

    Response result=await post(url,headers:requestHeaders,body:{
      "image": base64Image,
      "fileName":name+"."+fileExtension,
      "groupName":name,
    });
    print(jsonDecode(result.body));
    await prefs.setString('jwt',jsonDecode(result.body)['token']);
    return jsonDecode(result.body);
  }




  Future<Map> getLeaderBoardProfile()async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/getprofile";
    Response result=await get(url,headers:requestHeaders);
    // print(result.body);

    return jsonDecode(result.body);
    
  }

  Future<Map> avatarUpdate(int index)async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/avatarupdate";
    Response result=await post(url,headers:requestHeaders,body: {"avatar":index.toString()});
    // print(result.body);

    return jsonDecode(result.body);
    
  }


  Future<Map> userNameUpdate(String userName)async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/usernameupdate";
    Response result=await post(url,headers:requestHeaders,body: {"userName":userName});

    return jsonDecode(result.body);
    
  }

  Future<Map> getGroupDetail()async{
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${_comnvar.getUrl()}leaderBoard/getgroupdetail";
    Response result=await get(url,headers:requestHeaders);

    return jsonDecode(result.body);
    
  } 
  
  Future<Map> groupNameUpdate(String groupName,String gid)async{
    
    String url="${_comnvar.getUrl()}leaderBoard/groupnameupdate";
    print(gid+groupName);
    Response result=await post(url,body: {"groupName":groupName,"groupId":gid});
    return jsonDecode(result.body);
    // return new Map();
    
  }

  Future<Map> groupImageUpdate(File image,String gid,String name)async{
    
    String url="${_comnvar.getUrl()}leaderBoard/updategroupImage";
    String base64Image = base64Encode(image.readAsBytesSync());
    String fileExtension = (image.path.split("/").last).split('.').last;

    Response result=await post(url,body:{
      "image": base64Image,
      "fileName":name+"."+fileExtension,
      "groupId":gid,
    });
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  Future<Map> acceptGroupRequest(String index,String groupId)async{
    
    String url="${_comnvar.getUrl()}leaderBoard/acceptgrouprequest";
    

    Response result=await post(url,body:{
      "index": index,
      "groupId":groupId,
    });
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  
  Future<Map> removeMember(String index,String groupId)async{
    
    String url="${_comnvar.getUrl()}leaderBoard/removemember";
    
    Response result=await post(url,body:{
      "index": index,
      "groupId":groupId,
    });
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }
  

}


