import 'package:http/http.dart';
import 'dart:convert';
import 'comonvar.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:corsac_jwt/corsac_jwt.dart';

class UserService{
 CommoneVar comnvar=CommoneVar();
 
 Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  // login function
  Future<Map> login(String email,String password)async{    
    String url="${comnvar.getUrl()}user/login";
    var body={
      "email":email,
      "password":password,
    };
    Response result=await post(url,body:body);
    final SharedPreferences prefs = await _prefs;
    await prefs.setString('jwt', jsonDecode(result.body)['token'].toString());
    
    return jsonDecode(result.body);

  }

  // logout function
  Future logout()async{    
    
    final SharedPreferences prefs = await _prefs;
    await prefs.setString('jwt',"");    

  }

  // User Registration
  Future<Map> registration(Map userData) async {
    
    String url="${comnvar.getUrl()}user/register";
    
    Response result=await post(url,body:userData);

    
    if(jsonDecode(result.body)['status']){
      final SharedPreferences prefs = await _prefs;
      await prefs.setString('jwt', jsonDecode(result.body)['token']);
    }
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  // Get loged user data
  Future<Map> getUser() async {
    
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    String url="${comnvar.getUrl()}user/getUser";
    Response result=await get(url,headers: requestHeaders);
    Map userData=jsonDecode(result.body)['data'];
    print(jsonDecode(result.body)['data']['birthday']);
    String birthday=jsonDecode(result.body)['data']['birthday'];
    var temp=birthday.split('-');
    
    DateTime today=new DateTime.now();
    userData['age']=(today.year-int.parse(temp[0])).toString();
    return userData;
  }

  // update user Shedule
  Future<Map> updateShedule(Map data) async {
    
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');

    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/x-www-form-urlencoded',
      'Authorization':'Barear $token'
    };
    
    String url="${comnvar.getUrl()}user/sheduleUpdate";
    Response result=await post(url,body:data,headers:requestHeaders);
    
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  // Update userprofile 
   Future<Map> updateProfile(Map data) async {
    
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');

    Map<String, String> requestHeaders = {
      'Content-type': 'application/x-www-form-urlencoded',
      'Accept': 'application/x-www-form-urlencoded',
      'Authorization':'Barear $token'
    };
    String url="${comnvar.getUrl()}user/updateProfile";
    Response result=await post(url,body:data,headers:requestHeaders);
    // print(jsonDecode(result.body));
    prefs.setString('jwt',jsonDecode(result.body)['token']);
    print(jsonDecode(result.body));
    return jsonDecode(result.body);
  }

  Future<bool> isExist(String email) async {
    String url="${comnvar.getUrl()}user/isexist/"+email;
    Response result=await get(url);
    return jsonDecode(result.body)['status'];
  }

  // Check weather user is loged or not
  Future<bool> isLogedIn() async{    
    final SharedPreferences prefs = await _prefs;
    String token=(prefs.getString('jwt')).toString();
    print(token);
    print("hel");
    
    print(token=='null');
    if(token == 'null' || token ==""){
      return false;
    }else{
      return true;
    }
  }
  
  // Decode the jwt token
  Future<Map> decodeData() async{    
    final SharedPreferences prefs = await _prefs;
    String token=prefs.getString('jwt');
    print(token);
    if(token== "" || token==null){
       return new Map(); 
    }else{
      var decodedToken = new JWT.parse(token);
      print(decodedToken.claims['groupId']);
      return decodedToken.claims;
    }
    
  }
  // Decode the jwt token
  Future<Map> sendFeedBack(String type,String question) async{    
    Map user = await this.decodeData();
    
    String url="${comnvar.getUrl()}feedback/askquestion";
    Map data=new Map();
    data['userId']=user['id'];
    data['question']=question;
    data['type']=type;


    await post(url,body:data);
    
  }

// Decode the jwt token
  Future<Map> setToken(String token) async{    
    final SharedPreferences prefs = await _prefs;
    await prefs.setString('jwt',token);
  }
  

}