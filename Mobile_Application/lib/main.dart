import 'package:aqua/Widget/History/history.dart';
import 'package:aqua/Widget/Intro_page.dart';
import 'package:aqua/Widget/Leaderboard/leaderBoardedit.dart';
import 'package:aqua/Widget/Profile/editProfile.dart';
import 'package:aqua/Widget/signup/dailyactivitylevel.dart';
import 'package:flutter/material.dart';
import 'Widget/signup/signup.dart';
import 'Widget/signup/input_page.dart';
import 'Widget/signup/dailyRoutingSchedule.dart';
import 'Widget/signup/FinalStepStepper.dart';
import 'widget/dashboard.dart';
import 'Widget/Profile/profile.dart'; 
import 'Widget/login/login.dart';
import 'Widget/login/startup.dart';
import 'Widget/Leaderboard/leaderboard.dart';
import 'Widget/Profile/schedule.dart';
import 'Widget/WaterShedule/water_schedule.dart';
import 'Widget/LoadingPage.dart';
import 'Widget/feedbackk.dart';


void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      initialRoute: '/',
      routes: {
        '/': (context) => LoadingPage(),
        '/dasboard': (context) => Dashboard(title:'AQUA'),
        '/intro': (context) => IntroPage(),
        // '/loadingpage': (context) => LoadingPage(),
        '/leaderboard': (context) => LeaderBoard(),
        '/leaderboardEdit': (context) => LeaderBoardEdit(),
        '/profile': (context) => Profile(),
        '/history': (context) => History(),
        '/dailyschedule': (context) => Schedule(),
        '/waterschedule': (context) => WaterSchedule(),
        '/editProfile': (context) => EditProfile(),
        '/login': (context) => Login(),
        '/startup': (context) => Startup(),
        '/feedbackk': (context) => Feedbackk(),

        // registration routes
        '/register/1': (context) => new SignupPage(),
        '/register/2': (context) => InputPage(),
        '/register/3': (context) => DailyRoutingTime(),
        '/register/4': (context) => DailyActivityLevel(),
        '/register/5': (context) => FinalStep(),
      },
      title: 'AQUA',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      // home: IntroPage(),
      debugShowCheckedModeBanner: false,
    );
  }
}
class HexColor extends Color {
  HexColor(final String hexColor) : super(_getColorFromHex(hexColor));

  static int _getColorFromHex(String hexColor) {
    hexColor = hexColor.toUpperCase().replaceAll('#', '');
    if (hexColor.length == 6) {
      hexColor = 'FF' + hexColor;
    }
    return int.parse(hexColor, radix: 16);
  }
}
