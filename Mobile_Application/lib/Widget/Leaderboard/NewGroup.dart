import 'dart:async';
import 'dart:io';
import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/leaderboardService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:image_picker/image_picker.dart';

class NewGroup extends StatefulWidget {
  NewGroup({Key key}) : super(key: key);

  @override
  _NewGroupState createState() => _NewGroupState();
}

class _NewGroupState extends State<NewGroup>{

  bool loader=false;
  final groupName = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  CommoneVar commoneVar=new CommoneVar();
  LeaderBoardService leaderBoardService=new LeaderBoardService();
  bool load=false;
  File _image;
  final picker = ImagePicker();

  Future getImage() async {
    final pickedFile = await picker.getImage(source: ImageSource.gallery);

    setState(() {
      _image = pickedFile!=null?File(pickedFile.path):null;
    });
  }
  Future<Map> createGroup()async{
    if (_formKey.currentState.validate()){
      setState(() {
        load=true;
      });
      Map result=await leaderBoardService.creatGroup(_image,groupName.text.toString());
      return result;
    }
    return null;

  }
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Scaffold(
         backgroundColor: Colors.blue,
         appBar: AppBar(
           title: Text("Creat New Group"),
           elevation: 0,
           centerTitle: true,
          ),

      body:Container(
        child: Column(
          children: <Widget>[
            SizedBox(height:50.0),
            Expanded(
              child:Container(
              height: MediaQuery.of(context).size.height-131,
              decoration: BoxDecoration(
                color: Colors.white,
                borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0),topRight:Radius.circular(50.0)),
                ),
                child: ListView(
                  children: <Widget>[
                    SizedBox(height: 40,),
                    Stack(
                      children: <Widget>[
                        Center(
                          child: CircleAvatar(
                            backgroundImage:
                            _image==null?AssetImage('assets/empty.jpg'):
                            FileImage(_image),
                            radius: 80,
                          ),
                        ),
                        Align(
                          alignment: Alignment.bottomRight,
                          child:Container(
                            margin: EdgeInsets.only(right: (MediaQuery.of(context).size.width/2)-60),
                            
                          width: 30,
                          height: 30,
                          child: RawMaterialButton(
                            fillColor: Colors.blue,
                            shape: new CircleBorder(),
                                elevation: 0.0,
                                child: Icon(
                                  Icons.edit,
                                  color: Colors.white,
                                  size: 20,
                            ),
                            onPressed: getImage,
                          ),
                          ),
                        ),   
                      ],
                    ),
                    SizedBox(height: 50,),
                    Form(
                        key: _formKey,
                        child:Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.only(top: 30,left: 50,right: 50),
                              // decoration: BoxDecoration(
                              //   border: Border(bottom: BorderSide(color: Colors.red))
                              // ),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter group name';
                                  }
                                  return null;

                                },
                                controller: groupName,
                                decoration: InputDecoration(
                                  border: new OutlineInputBorder(
                                    borderRadius: const BorderRadius.all(
                                      const Radius.circular(10.0),
                                    ),
                                  ),
                                  hintText: "Group Name",
                                  hintStyle: TextStyle(color: Colors.blue[700])
                                )
                              ),
                            ),
                            
                          ]
                        ),
                      ),
                      Padding(
                        padding: const EdgeInsets.only(left: 100,right: 100,top:40),
                        child:Column(
                          children: <Widget>[
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                                ),
                              height: 55,
                              child:
                              Builder(
                                builder: (context)=>
                                  FlatButton(
                                    color: Colors.blue,
                                    onPressed: ()async {
                                      Map result=await createGroup();
                                      // print("object");
                                      if(result==null){

                                      }else if(result['status']==true){
                                      Timer(Duration(seconds: 3), () => Navigator.pop(context));
                                        
                                        Scaffold.of(context).showSnackBar(
                                          SnackBar(
                                            content:ListTile(
                                              title: Text("Successfully Created"),
                                              trailing:Icon(Icons.beenhere)
                                            ) ,
                                          backgroundColor: Colors.green,
                                          )
                                        );
                                      }else{
                                        Scaffold.of(context).showSnackBar(
                                          SnackBar(
                                            content:ListTile(
                                              title: Text(result['msg'].toString()),
                                              trailing:Icon(Icons.announcement)
                                            ) ,
                                          backgroundColor: Colors.red,
                                          )
                                        );
                                      }
                                      setState(() {
                                        load=false;
                                      });
                                      
                                    },      
                                    child: Center(
                                      child:load?
                                        SpinKitThreeBounce(color: Colors.white,size: 45.0,):
                                        Text("Create", style: TextStyle(color: Colors.white,fontSize: 20,),
                                      ),
                                    ),
                                  ),
                                
                              )
                              
                            )
                          ]
                            ),
                      ),
                    // Center(
                    
                    //   child:Text("jjjjjjjjjj") ,
                    // )
                  ],
                ),
              )
            ),
        ],
        )
      )

         
    ),
    );
  }
}