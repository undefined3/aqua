

import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Widget/Leaderboard/FindGroup.dart';
import 'package:aqua/Widget/Leaderboard/LeadBoardGroup.dart';
import 'package:aqua/Widget/Leaderboard/NewGroup.dart';
import 'package:aqua/Widget/Leaderboard/leaderboardprofile.dart';
import 'package:flutter/material.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';





class LeaderBoardEdit extends StatefulWidget {
  LeaderBoardEdit({Key key}) : super(key: key);

  @override
  _LeaderBoardEditState createState() => _LeaderBoardEditState();
}

class _LeaderBoardEditState extends State<LeaderBoardEdit> {

    UserService service;
    CommoneVar commoneVar=new CommoneVar();
    Map userData;
    bool loader=true;
    bool update=false;
    final name = TextEditingController();
    final weight = TextEditingController();
    final height =TextEditingController();
    final _formKey = GlobalKey<FormState>();


    @override
  void initState() {
    // TODO: implement initState
    super.initState();
    service=new UserService();
  }
  
  @override
  Widget build(BuildContext context) {
    return DefaultTabController(
        length: 2,
        initialIndex: 0,
        child: Scaffold(
          backgroundColor: Colors.blue,
          appBar: AppBar(
            actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (value)async => {
              await Navigator.push(context,
               MaterialPageRoute(builder: (context) =>NewGroup()),
              )
            },
            itemBuilder: (BuildContext context) {
              return [PopupMenuItem<String>(
                value: "newGroup",
                child: Text("Create New Group"),
              )];
            },
          )
        ],
            elevation: 10,
              title: Text("Leadboard",style: TextStyle(fontSize: 22),),
              // backgroundColor: Colors.deepPurple[900],
              centerTitle: true,
              bottom: TabBar(
                onTap:(value)async=>{},
                indicatorColor: Colors.blue,
                // indicatorWeight:2,
                tabs:[
                    Tab(child: Icon(Icons.person)),
                    Tab(child: Icon(Icons.people)),
                  ],
                ),
            ),
            body:TabBarView(
              children: <Widget>[
                LeadboardProfile(),
                LeadboardGroup(),
              ],)
        )
      
    );
  }
}




// class LeaderBoardEdit extends StatelessWidget {
//   const LeaderBoardEdit({Key key}) : super(key: key);

//   @override
//   Widget build(BuildContext context) {
//     return  
//   }
// }



