import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/leaderboardService.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:aqua/Widget/Leaderboard/FindGroup.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'dart:io';
import 'package:image_picker/image_picker.dart';
import 'package:flutter/services.dart';


class LeadboardGroup extends StatefulWidget {
  LeadboardGroup({Key key}) : super(key: key);

  @override
  _LeadboardGroupState createState() => _LeadboardGroupState();
}

class _LeadboardGroupState extends State<LeadboardGroup> {

  CommoneVar commoneVar=new CommoneVar();
  final groupName = TextEditingController();
  LeaderBoardService leaderBoardService=new LeaderBoardService();
  UserService userService=new UserService();
  Map groupDetail;
  bool mainspinner=true;
  bool groupNameSpinner=false;
  bool isAdmin=false;
  final _formKey = GlobalKey<FormState>();
  File _image;
  final picker = ImagePicker();
  bool hasgroup=false;

  @override
  void initState() {

    // TODO: implement initState
    super.initState();
    hasGroup();
    
  }
  void hasGroup()async{
    Map data=await userService.decodeData();
    print(data);
    if(data['groupId'].toString()=="null"){

      setState(() {
        hasgroup=false;
        mainspinner=false;
      });
    }else{
      setState(() {
        hasgroup=true;
      });
      getGroupDetail();
    }

    
  }
  void getGroupDetail()async{
    Map result=await leaderBoardService.getGroupDetail();
    print(result);
    if(result['status']==true){
      Map temp=await userService.decodeData();
      for(int i=0;i<result['data']['members'].length;i++){
        result['data']['members'][i]['aspinner']=false;
        result['data']['members'][i]['rspinner']=false;

      }
      print(result['data']['members']);
      if(result['data']['admin']==temp['id']){
        setState(() {
          isAdmin=true;
        });
      }else{
        setState(() {
          isAdmin=false;
        });
      }
      setState(() {
        groupDetail=result['data'];
        mainspinner=false;
      });
    }
    print(isAdmin);
  }

  void updateGroupName(BuildContext context)async{


    setState(() {
      groupNameSpinner=true;
    });




    print(groupDetail);
    Map result=await leaderBoardService.groupNameUpdate(groupName.text, groupDetail['_id']);
    if(result['status']==true){
      Map temp=groupDetail;
      temp['groupName']=groupName.text;
      setState(() {
        groupDetail=temp;
        groupNameSpinner=false;
      });
      Navigator.of(context).pop();
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content:ListTile(
            title: Text("Successfully Updated"),
            trailing:Icon(Icons.beenhere)
          ) ,
        backgroundColor: Colors.green,
        )
      );
    }
  }
  Future getImage() async {
      final pickedFile = await picker.getImage(source: ImageSource.gallery);
      
      setState(() {
        _image = pickedFile!=null?File(pickedFile.path):null;
      });
  }
  void updateImage(BuildContext context)async{

    Map result=await leaderBoardService.groupImageUpdate(_image,groupDetail["_id"],groupDetail["groupName"]);
    if(result["status"]==true){
      Map temp=groupDetail;
      temp['image']=result["data"]['image'];
      imageCache.clear();
      setState((){
        groupDetail=temp;
        _image=null;  
      });
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content:ListTile(
            title: Text("Successfully Updated"),
            trailing:Icon(Icons.beenhere)
          ) ,
        backgroundColor: Colors.green,
        )
      );
    } 
  }

  void groupRequestAccept(int index,BuildContext context)async{
    Map temp=groupDetail;
    
    temp['members'][index]['aspinner']=true;
    setState(() {
      groupDetail=temp;
    });
    Map result=await leaderBoardService.acceptGroupRequest(index.toString(),groupDetail["_id"]);

    if(result['status']==true){

      Map temp=groupDetail;
      temp['members'][index]['aspinner']=false; 
      temp['members'][index]['status']=true; 
      setState((){
        groupDetail=temp;
      });
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content:ListTile(
            title: Text("Successfully Updated"),
            trailing:Icon(Icons.beenhere)
          ),
        backgroundColor: Colors.green,
        )
      );
    }
  }
  void removeMember(int index,BuildContext context)async{
    Map temp=groupDetail;
    
    temp['members'][index]['rspinner']=true;
    setState(() {
      groupDetail=temp;
    });
    Map result=await leaderBoardService.removeMember(index.toString(),groupDetail["_id"]);

    if(result['status']==true){

      temp=groupDetail;
      List oldmem=temp['members'];
      List nemem=new List();

      for(int i=0;i<oldmem.length;i++){
        if(index==i){
          continue;
        } 
        nemem.add(oldmem[i]);
      }
      temp['members']=nemem;
      // temp['members'][index]['rspinner']=false; 
      // temp['members'][index]['status']=true; 
      setState((){
        groupDetail=temp;
      });
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content:ListTile(
            title: Text("Successfully Updated"),
            trailing:Icon(Icons.beenhere)
          ),
        backgroundColor: Colors.green,
        )
      );
    }
  }
  @override
  Widget build(BuildContext context) {
    return mainspinner? 
    SpinKitWanderingCubes(
      color: Colors.white,
      size: 50.0,
    ):
    !hasgroup?
    FindGroup():
    Column(
      children: <Widget>[
        SizedBox(height:20.0),
        Padding(
          padding: EdgeInsets.only(left:0.0),
          child:Center(
            child:Text(groupDetail['groupName'].toString(),
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0
                )
              ))
        ),
        SizedBox(height:50.0),
        Expanded(
          child:Container(
          // height: 400,
          height: MediaQuery.of(context).size.height-227,
          decoration: BoxDecoration(
            color: Colors.white,
            borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0),topRight:Radius.circular(50.0) ),
          ),
          child: ListView.builder(
            itemCount: groupDetail['members'].length+1,
            itemBuilder: (context1,index){
              if(index==0){
                return  Padding(
                padding: EdgeInsets.only(top:55.0),
                child: Container(
                  // height:450,
                  child:Column(
                    children: <Widget>[
                      Container(
                        width: 120.0,
                        height: 120.0,
                        child: new Stack(children: <Widget>[
                          CircleAvatar(
                            backgroundImage:_image==null? NetworkImage(commoneVar.getUrl()+"getfile/groups/"+groupDetail["image"].toString()):FileImage(_image),
                            radius: 80,
                          ),
                          isAdmin?Builder(
                            builder: (context2)=>Align(
                            alignment: Alignment.topRight,
                            child:Container(
                            width: 30,
                            height: 30,
                            child: RawMaterialButton(
                              fillColor: Colors.blue,
                              shape: new CircleBorder(),
                                  elevation: 0.0,
                                  child: Icon(
                                    Icons.edit,
                                    color: Colors.white,
                                    size: 20,
                                  ),
                                  onPressed:getImage
                            ),
                            
                            ),
                          ),
                          ):Text(""),
                          

                        ],
                        ),
                      ),
                      _image!=null?Builder(
                          builder: (context2)=>Align(
                          alignment: Alignment.bottomCenter,
                          child:FlatButton (  
                              shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(10.0),
                                side: BorderSide(color: Colors.blue)
                              ),
                              color: Colors.blue[400],
                              onPressed: () {
                                /*...*/
                                updateImage(context);
                              },
                              child: Text(
                                "Update",
                                style: TextStyle(color: Colors.white),
                              ),
                            ),
                        ),
                      ):Text(""),
                      SizedBox(height: 30,),
                      
      /*+++++++++++++++++++++++++++++++ Group detail++++++++++++++++++++++++++++++++++++++++++*/ 

                      Row(children: <Widget>[
                        SizedBox(width: 20,),
                        Icon(Icons.person,color: Colors.blue,),
                        SizedBox(width: 20,),
              /*+++++++++++++++++++++++ Group Name +++++++++++++++++++*/ 

                        Text("Group Name",style:TextStyle(color: Colors.blue,fontSize: 20),),
                        SizedBox(width: 20,),
                        Text(groupDetail['groupName'].toString()),
                    /*++++++++++++++++ Group Name edit icon  and edit dialog ++++++++++++++++*/ 

                        isAdmin?
                        IconButton(
                          icon:Icon(Icons.edit),
                          iconSize: 20,
                          onPressed: ()=>{
                            showDialog(
                              context: context,
                              builder: (BuildContext context1){
                                return AlertDialog(
                                  title: new Text("Change Group Name"),
                                  content:groupNameSpinner?
                                    Container(
                                       height: 60,
                                       child: Center(
                                        child:SpinKitCircle(
                                        color: Colors.lightBlue,
                                        size: 50.0
                                        )
                                      ),
                                     ):
                                  Form(
                                    key: _formKey,
                                    child:
                                     TextFormField(
                                      controller:groupName,
                                      validator: (value) {
                                        print(value);
                                          if(value.length==0){
                                            return 'Please enter your new name';
                                          }else{
                                            return null;
                                          }
                                        },
                                      decoration: new InputDecoration(
                                        focusedBorder: OutlineInputBorder(
                                          borderSide: BorderSide(color: Colors.blue[200],width:5.0),
                                        ),
                                        enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[300],width: 2.0)),
                                        //labelText: 'Email Address',
                                        hintText: "Enter your new Group Name",
                                      ),
                                    ),
                                  ),
                                  actions: <Widget>[
                                    // usually buttons at the bottom of the dialog
                                    new FlatButton(
                                      child: new Text("Close"),
                                      onPressed: () {
                                        Navigator.of(context).pop();
                                      },
                                    ),
                                    new FlatButton(
                                      child: new Text("Update"),
                                      onPressed: () {
                                        if (_formKey.currentState.validate()){
                                            updateGroupName(context);
                                          }
                                        // Navigator.of(context).pop();
                                      },
                                    ),
                                  ],
                                );
                              }
                            )
                          },
                          ):Text("")

      /*+++++++++++++++++++++++++++++++ NUmber of members start++++++++++++++++++++++++++++++++++++++++++*/ 

                      ],),
                      SizedBox(height: 30,),
                      Row(children: <Widget>[
                        SizedBox(width: 20,),
                        Icon(Icons.group,color: Colors.blue,),
                        SizedBox(width: 20,),
                        Text("Number Of Members",style:TextStyle(color: Colors.blue,fontSize: 20),),
                        SizedBox(width: 30,),
                        Text(groupDetail['members'].length.toString())
                      ],),
                        SizedBox(height: 50,),
      /*+++++++++++++++++++++++++++++++ NUmber of members end++++++++++++++++++++++++++++++++++++++++++*/ 

      /*+++++++++++++++++++++++++++++++Group leav Button start++++++++++++++++++++++++++++++++++++++++++*/ 

                        !isAdmin?FlatButton (
                          shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(14.0),
                            side: BorderSide(color: Colors.red)
                          ),
                          color: Colors.red[400],
                          onPressed: () {
                            /*...*/
                          },
                          child: Text(
                            "Leave From Group",
                            style: TextStyle(color: Colors.white),
                          ),
                        ):Text(""),
                    ],),
                ),
              );
              }else if(!groupDetail['status'] &&index==1){
                return Center(child: Text("Your Request is still pending"));
              }else if(index==1){
                return Card(
                margin: EdgeInsets.fromLTRB(10, 30, 0, 0),
                child: ListTile(
                    title: Text(groupDetail['members'][index-1]['name']),
                    trailing: Padding(
                              padding: EdgeInsets.only(right:70.0),
                              child: Text("Admin"),
                    ),
                    
                    leading:CircleAvatar(
                          backgroundImage:
                          AssetImage(
                            groupDetail['members'][index-1]['gender']=="M"?
                            commoneVar.avatarMale[
                              (int.parse(groupDetail['members'][index-1]['avatar'])+1).toString()].toString():
                            commoneVar.avatarFemale[
                              (int.parse(groupDetail['members'][index-1]['avatar'])+1).toString()].toString()),
                    ),
                    
                          
                  )
                );
              }

      /*+++++++++++++++++++++++++++++++Group leav Button end++++++++++++++++++++++++++++++++++++++++++*/ 
      

      /*+++++++++++++++++++++++++++++++Member list cards start++++++++++++++++++++++++++++++++++++++++++*/ 

              return
              !groupDetail['status']?
              Center(child: Text(""))
              : 
              Card(
                margin: EdgeInsets.fromLTRB(10, 30, 0, 0),
                child: ListTile(
                    title: Text(groupDetail['members'][index-1]['name']),
                    trailing:isAdmin?

                    // Member remove button
                      (groupDetail['members'][index-1]['status']?
                        RawMaterialButton(
                              fillColor: Colors.red[400],
                              shape: new CircleBorder(),
                                  elevation: 0.0,
                                  child:groupDetail['members'][index-1]['rspinner']?
                                    Container(child:SpinKitThreeBounce(color: Colors.white,size:20,),width: 50,height: 20,):
                                   Icon(
                                      Icons.delete,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                  onPressed: () => {
                                    removeMember(index-1,context)
                                  }
                        )
                        // new member accept or remove
                        :
                        Container(
                          width:180,
                          child: Row(children: <Widget>[
                              RawMaterialButton(
                                fillColor: Colors.green,
                                shape: new CircleBorder(),
                                    elevation: 0.0,
                                    child:groupDetail['members'][index-1]['aspinner']?
                                      Container(child:SpinKitThreeBounce(color: Colors.white,size:20,),width: 50,height: 20,):
                                      Icon(
                                        Icons.group_add,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    onPressed: () => {
                                      groupRequestAccept(index-1,context)
                                    },
                              ),
                              RawMaterialButton(
                                fillColor: Colors.red[400],
                                shape: new CircleBorder(),
                                    elevation: 0.0,
                                    child:groupDetail['members'][index-1]['rspinner']?
                                      Container(child:SpinKitThreeBounce(color: Colors.white,size:20,),width: 50,height: 20,):
                                    Icon(
                                        Icons.delete,
                                        color: Colors.white,
                                        size: 20,
                                      ),
                                    onPressed: () => {
                                      removeMember(index-1,context)
                                    }
                              ),
                          ],),
                        )
                    ):Text(""),

                    onTap:(){
                    },
                    leading:CircleAvatar(
                          backgroundImage:AssetImage(
                            groupDetail['members'][index-1]['gender']=="M"?
                            commoneVar.avatarMale[
                              (int.parse(groupDetail['members'][index-1]['avatar'])+1).toString()].toString():
                            commoneVar.avatarFemale[
                              (int.parse(groupDetail['members'][index-1]['avatar'])+1).toString()].toString()),
                    ),
                    
                          
                  )
              );
      /*+++++++++++++++++++++++++++++++Member list cards end++++++++++++++++++++++++++++++++++++++++++*/ 
            }
          )
        ),
      ),
        
      ]
    );
  }
}