import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/leaderboardService.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class LeadboardProfile extends StatefulWidget {
  LeadboardProfile({Key key}) : super(key: key);

  @override
  _LeadboardProfileState createState() => _LeadboardProfileState();
}

class _LeadboardProfileState extends State<LeadboardProfile> {
  GlobalKey<ScaffoldState> _scaffoldKey = GlobalKey<ScaffoldState>();
  CommoneVar commoneVar=new CommoneVar();
  final userName = TextEditingController();
  LeaderBoardService leaderBoardService=new LeaderBoardService();
  UserService userService =new UserService();
  bool spinner=true;
  bool spinnerUserName=false;
  Map userData;
  bool gender=false;
  final _formKey = GlobalKey<FormState>();
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    getUserData();

  }

  void getUserData()async{
    Map result=await leaderBoardService.getLeaderBoardProfile();
    Map user=await userService.decodeData();
    print(user);
    if(user['gender']=='M'){
      setState(() {
        gender=true;
      });
    }
    if(result['status']==true){

      print(result["data"]);
      setState(() {
        userData=result['data'];
        spinner=false;
      });
    }
    
  }
  void avatarUpdate(int index,BuildContext context)async{
    Map temp=userData;
    temp['avatar']=index.toString();
    setState(() {
     userData=temp; 
    });
    print(index);
    print(context);
    Navigator.pop(context);
    Scaffold.of(context).showSnackBar(
      SnackBar(
        content:ListTile(
          title: Text("Successfully changed avatar"),
          trailing:Icon(Icons.beenhere)
        ) ,
      backgroundColor: Colors.green,
      )
    );
    await leaderBoardService.avatarUpdate(index);
  }

  void updateUsername(BuildContext context)async{
    setState(() {
      spinnerUserName=true;
    });
    Map result = await leaderBoardService.userNameUpdate(userName.text);
    if(result['status']==true){
      Map temp=userData;
      temp['userName']=userName.text;
      setState(() {
        spinnerUserName=false;
        userData=temp;
      });
      Scaffold.of(context).showSnackBar(
      SnackBar(
        content:ListTile(
          title: Text("Successfully User Name Updated!!!"),
          trailing:Icon(Icons.beenhere)
        ) ,
      backgroundColor: Colors.green,
      )
    );
    Navigator.of(context).pop();
      
    }
    print(userName.text);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.blue[400],
      body:
      Column(
        children: <Widget>[
          SizedBox(height:30.0),
          Padding(
            padding: EdgeInsets.only(left:40.0),
            child:Center(
              child:Text('Leadboard Profile',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0
                  )
                ))
          ),
          SizedBox(height:20.0),
          
          Expanded(
            child:Container(
            // height: 400,
            height: MediaQuery.of(context).size.height-227,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0),topRight:Radius.circular(50.0) ),
            ),
            child:spinner?Center(child:SpinKitWanderingCubes(
                      color: Colors.lightBlue,
                      size: 50.0,)
                    ): 
            ListView(
              primary: false,
              padding: EdgeInsets.only(left:25.0, right:20.0),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top:55.0),
                  child: Container(
                    height:550,
                    child:Column(
                      children: <Widget>[
                        Container(
                          width: 120.0,
                          height: 120.0,
                          child: new Stack(children: <Widget>[
                            CircleAvatar(
                              backgroundImage:
                                AssetImage(
                                  gender?
                                  commoneVar.avatarMale[(int.parse(userData['avatar'])+1).toString()].toString():
                                  commoneVar.avatarFemale[(int.parse(userData['avatar'])+1).toString()].toString()),
                              radius: 80,
                            ),
                            Builder(
                              builder: (context)=>Align(
                              alignment: Alignment.topRight,
                              child:Container(
                              width: 30,
                              height: 30,
                              child: RawMaterialButton(
                                fillColor: Colors.blue,
                                shape: new CircleBorder(),
                                    elevation: 0.0,
                                    child: Icon(
                                      Icons.edit,
                                      color: Colors.white,
                                      size: 20,
                                    ),
                                    onPressed:(){
                                      showModalBottomSheet<void>(context: context, builder: (BuildContext context0) {
                                          return Container(
                                            // height: 200,
                                            child:Padding(
                                              padding: const EdgeInsets.only(top: 100,bottom: 100),
                                              child: ListView.builder( 
                                                  shrinkWrap: true,
                                                  scrollDirection: Axis.horizontal,
                                                  itemCount: commoneVar.avatarMale.length,
                                                  itemBuilder: (BuildContext context1, int index) =>
                                                  GestureDetector(
                                                    child: CircleAvatar(
                                                          backgroundImage:
                                                           AssetImage(
                                                             gender?
                                                             commoneVar.avatarMale[(index+1).toString()].toString():
                                                             commoneVar.avatarFemale[(index+1).toString()].toString()
                                                             ),
                                                          radius: 80,
                                                          // commoneVar.avatarMale[(index+1).toString()].toString()
                                                        ),
                                                        onTap: () => {
                                                          avatarUpdate(index,context),
                                                          
                                                        },
                                                  )
                                                  
                                                ), 
                                            ),
                                          );
                                        });
                                    }
                              ),
                              
                              ),
                            ),
                            ),
                            
                          ],
                          ),
                        ),
                        
                        SizedBox(height: 30,),
                        Row(children: <Widget>[
                          SizedBox(width: 40,),
                          Icon(Icons.person,color: Colors.blue,),
                          SizedBox(width: 20,),
                          Text("Username",style:TextStyle(color: Colors.blue,fontSize: 20),),
                          SizedBox(width: 50,),
                          Text(userData['userName'].toString()),
                          IconButton(
                            icon:Icon(Icons.edit),
                            iconSize: 20,
                            onPressed: ()=>{
                              showDialog(
                                context: context,
                                builder: (BuildContext context1){
                                  return AlertDialog(
                                    title: new Text("Change Your Username"),
                                    content:spinnerUserName?
                                     Container(
                                       height: 60,
                                       child: Center(
                                        child:SpinKitCircle(
                                        color: Colors.lightBlue,
                                        size: 50.0
                                        )
                                      ),
                                     ): 
                                    Form(
                                      key: _formKey,
                                      child: TextFormField(
                                        controller:userName,
                                        validator: (value) {
                                          if (value.length>10) {
                                            return 'Your User Name Maximum lenght is 10';
                                          }else if(value.length==0){
                                            return 'Please enter your new name';
                                          }
                                          else{
                                            return null;
                                          }
                                        },
                                        decoration: new InputDecoration(
                                          focusedBorder: OutlineInputBorder(
                                            borderSide: BorderSide(color: Colors.blue[200],width:5.0),
                                          ),
                                          enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[300],width: 2.0)),
                                          //labelText: 'Email Address',
                                          hintText: "Enter your new username",
                                        ),
                                      ),
                                    ),
                                    actions: <Widget>[
                                      // usually buttons at the bottom of the dialog
                                      new FlatButton(
                                        child: new Text("Close"),
                                        onPressed: () {
                                          // userName.clear();
                                          Navigator.of(context).pop();
                                        },
                                      ),
                                      new FlatButton(
                                        child: new Text("Update"),
                                        onPressed: () {
                                          if (_formKey.currentState.validate()){
                                            updateUsername(context);
                                          }
                                        },
                                      ),
                                    ],
                                  );
                                }
                              )
                            },
                            )
                        ],),
                        SizedBox(height: 30,),
                        Row(children: <Widget>[
                          SizedBox(width: 40,),
                          Icon(Icons.group,color: Colors.blue,),
                          SizedBox(width: 20,),
                          Text("Group Name",style:TextStyle(color: Colors.blue,fontSize: 20),),
                          SizedBox(width: 30,),
                          Text(userData['groupName'].toString())
                        ],),

                        SizedBox(height: 30,),
                        Row(children: <Widget>[
                          SizedBox(width: 40,),
                          Icon(Icons.filter_frames,color: Colors.blue,),
                          SizedBox(width: 20,),
                          Text("I Earned",style:TextStyle(color: Colors.blue,fontSize: 20),),
                          SizedBox(width: 70,),
                          Text(userData['score'].toString())
                        ],),
                        
                        SizedBox(height: 30,),
                        // Row(children: <Widget>[
                        //   SizedBox(width: 40,),
                        //   Icon(Icons.assessment,color: Colors.blue,),
                        //   SizedBox(width: 20,),
                        //   Text("My Rank",style:TextStyle(color: Colors.blue,fontSize: 20),),
                        //   SizedBox(width: 67,),
                        //   Text(userData['rank'].toString())
                        // ],),
                        SizedBox(height: 10,),
                        Image(
                          height: 100,
                              image: AssetImage(
                                'assets/images/badges/${userData['badge']}.jpg'),
                        ),
                        SizedBox(height: 20),
                        // details about awards
                        Text("Click to see how to get these awards" , style: TextStyle(
        fontSize: 15,
        color: Colors.blue[300],
      ),),
                        Row(
  crossAxisAlignment: CrossAxisAlignment.center,
  children: [
    Expanded(
      child: InkWell(
           onTap: () => _popupDialog(context ,'Bronze award ' , 'You need 25 points to get this'),
          child: Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset('assets/images/badges/0.jpg', width: 50,height: 50),
            ),),
        )
    ),
    Expanded(
      child: InkWell(
           onTap: () => _popupDialog(context ,'Silver award ' , 'You need 50 points to get this'),
          child: Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset('assets/images/badges/1.jpg', width: 50,height: 50),
            ),),
        )
    ),
    Expanded(
      child: InkWell(
           onTap: () => _popupDialog(context ,'Gold award ' , 'You need 100 points to get this'),
          child: Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset('assets/images/badges/2.jpg', width: 50,height: 50),
            ),),
        )
    ),
     Expanded(
      child: InkWell(
           onTap: () => _popupDialog(context ,'Platinum award ' , 'You need 150 points to get this'),
          child: Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset('assets/images/badges/5.jpg', width: 50,height: 50),
            ),),
        )
    ),
      Expanded(
      child: InkWell(
           onTap: () => _popupDialog(context ,'Aqua Premium award ' , 'You need 500 points to get this'),
          child: Container(
            child: ClipRRect(
              borderRadius: BorderRadius.circular(20.0),
              child: Image.asset('assets/images/badges/3.jpg', width: 50,height: 50),
            ),),
        )
    ),
  ],
),
                        
                        //details about awards
                      ],),
                  ),
                ),
                  
                
              ],
            )
          ),
        ),
          
        ]
      )  
    );
  }
}

void _popupDialog(BuildContext context , String topic , String details) {
    showDialog(
        context: context,
        builder: (context) {
          return AlertDialog(
            title: Text(topic),
            content: Text(details),
            actions: <Widget>[
              FlatButton(
                  onPressed: () => Navigator.of(context).pop(),
                  child: Text('OK')),
             
            ],
          );
        });
  }
