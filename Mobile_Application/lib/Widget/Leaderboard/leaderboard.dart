import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter/material.dart';
import './mymark.dart';
import '../../Services/leaderboardService.dart';
import 'dart:async';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LeaderBoard extends StatefulWidget {
  @override
  _LeaderBoardState createState() => _LeaderBoardState();
  
}

class _LeaderBoardState extends State<LeaderBoard> with SingleTickerProviderStateMixin{
  // deckare leaderboard variable
  LeaderBoardService service;

  CommoneVar commoneVar=new CommoneVar();
  UserService userService=new UserService();
  Map mydata;

  // Variable for store friends data
  dynamic friend=[];

  // Loader variable
  bool load=true,hasGroup=true;

  // Tab controler
  TabController _tabController;

  // Asynronus  function for Get leaderboard data
  Future<void> setFriend(String marktype) async {
    // First check user has a group or not

    Map token=await userService.decodeData();
    if(token['groupId'].toString()=="null"){
      setState(() {
        hasGroup=false;
        load=false;
      });
      return;
    }

    // First set loader varible true 
    setState(() {
        this.load=true;
    });
    // Call the service and get data from bancend server
    Map temp=await service.getFriend(marktype);
    print(temp['status']);
    if(temp['status']){
    // After data retrive set retrived data to friend variable

      setState(() {
        this.friend=temp['data'];
        this.load=false;
      });
      for(int i=0;i<temp['data'].length;i++){
        if(token['id']==temp['data'][i]['id']){
          print(token['groupId']);
          setState(() {
            mydata=temp['data'][i];
          });
        }
      }
    }else{
      if(temp['hasGroup']==false){
        setState(() {
          hasGroup=false;
          load=false;
        });
        await userService.setToken(temp['token']);
      }
    }
  }

  @override
  void initState(){ 
    super.initState();

    // make a LeaderBoardService instance
    service=LeaderBoardService();    
    //call the setFriend funtion to get friends data 
    setFriend("daily");
    _tabController = TabController(vsync:this,length:3);
    _tabController.addListener(_handleTabSelection);
    
  }
  void _handleTabSelection(){
    print("test");
    if (_tabController.indexIsChanging) {
      _tabController.index==0?this.setFriend("weekly"):_tabController.index==1?this.setFriend("daily"):this.setFriend("monthly");
    }
  }
  @override
  Widget build(BuildContext context) {
    
    // making 3 tab widget to display daily weekly and monthly data
    return DefaultTabController(
          length: 3,
          initialIndex: 1,
          child:Scaffold(
            backgroundColor:Colors.white,
            
            // float Action button to edit relevant leaderboard data 
            floatingActionButton: FloatingActionButton(
              onPressed:()=>{
                Navigator.pushNamed(context,'/leaderboardEdit')
              },
              child: Icon(Icons.person),
            ),

            //App bar of leaderboard  
            appBar: AppBar(
              title: Text("Leadboard",style: TextStyle(fontSize: 22),),
              // backgroundColor: Colors.deepPurple[900],
              centerTitle: true,
              bottom: TabBar(
                onTap:(value)async=>{
                  await (value==0?this.setFriend("weekly"):value==1?this.setFriend("daily"):this.setFriend("monthly"))
                },
                indicatorColor: Colors.white,
                indicatorWeight:2,
                tabs:[
                    Tab(child: Text("Weekly",style: TextStyle(color: Colors.white))),
                    Tab(child: Text("Today",style: TextStyle(color: Colors.white)),),
                    Tab(child: Text("Monthly",style: TextStyle(color: Colors.white)),),
                  ],
                ),
            ),
            // backgroundColor: Colors.blue[100],
            body:load?Center(child:SpinKitWanderingCubes(
                    color: Colors.lightBlue,
                    size: 50.0,)
                  ):
                TabBarView( 
                  children: [

                    !hasGroup?Column(
                      children:<Widget>[
                        SizedBox(height: 50,),

                        Text("You don't have a group."),
                        Text("Please add to existing one "),
                        Text(" or "),
                        Text("create new group and add new members"),
                        Container(
                          padding: EdgeInsets.only(top: 50),
                          height: 200,
                          child:Image(
                            image: AssetImage("assets/images/sad.png"),
                          ), 
                          
                        )
                        
                        
                      ] 
                    ):
                    ListView.builder(
                      itemCount: friend.length+2,
                      itemBuilder: (context,index){
                        if(index==0){
                          return  MyMark(textval:"You have ${mydata['score']} points " ,mark:(double.parse(mydata["score"])/100*5));
                        }
                        if(index==friend.length+1){
                          return (
                            SizedBox(height: 60,)
                          );
                        }
                        return Card(
                          margin: EdgeInsets.fromLTRB(10, 30, 0, 0),
                          child: ListTile(
                              // selected: friend[index-1]["name"]=="Pradeepa"?true:false,
                              title: Text(friend[index-1]["name"]),
                              trailing:Image(image:AssetImage('assets/images/badges/${friend[index-1]["badge"]}.jpg')),
                              // subtitle: FriendMark(mark: (friend[index-1]["score"]/100*5)),
                              onTap:(){},
                              leading:CircleAvatar(
                                backgroundImage: AssetImage(
                                  friend[index-1]["gender"]=='M'?
                                  commoneVar.avatarMale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString():
                                  commoneVar.avatarFemale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString()
                                ),
                              ),
                              subtitle: Column(
                                children: <Widget>[
                                  FriendMark(mark: (double.parse(friend[index-1]["score"])/100*5)),
                                  Text(
                                    "${friend[index-1]["score"]}/100",
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontSize:20,
                                    ),
                                  )
                                ]
                                ),
                                    
                            )
                        );
                      }
                      ),
                      !hasGroup?Column(
                      children:<Widget>[
                          SizedBox(height: 50,),
                          
                          Text("You don't have a group."),
                          Text("Please add to existing one "),
                          Text(" or "),
                          Text("create new group and add new members"),
                          Container(
                            padding: EdgeInsets.only(top: 50),
                            height: 200,
                            child:Image(
                              image: AssetImage("assets/images/sad.png"),
                            ), 
                            
                          )
                          
                          
                        ] 
                      ):
                      ListView.builder(
                      itemCount: friend.length+2,
                      itemBuilder: (context,index){
                        if(index==0){
                          return  MyMark(textval:"You have ${mydata['score']} points" ,mark: (double.parse(mydata["score"])/100*5));
                        }
                        if(index==friend.length+1){
                          return (
                            SizedBox(height: 60,)
                          );
                        }
                        return Card(
                          child: ListTile(
                              // selected: friend[index-1]["name"]=="Sandaruwan"?true:false,
                              title: Text(friend[index-1]["name"]),
                              
                              trailing:Image(image:AssetImage('assets/images/badges/${friend[index-1]["badge"]}.jpg')),
                              subtitle: Column(
                                children: <Widget>[
                                  FriendMark(mark:(double.parse(friend[index-1]["score"])/100*5) ),
                                  Text(
                                    "${friend[index-1]["score"]}/100",
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontSize:20,
                                    ),
                                  )
                                ]
                                ),
                              onTap:(){},
                              leading:CircleAvatar(
                                backgroundImage: AssetImage(
                                  friend[index-1]["gender"]=='M'?
                                  commoneVar.avatarMale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString():
                                  commoneVar.avatarFemale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString()
                                ),
                              ),
                                    
                            )
                        );
                      }
                      ),
                      !hasGroup?Column(
                        children:<Widget>[
                          SizedBox(height: 50,),
                          
                          Text("You don't have a group."),
                          Text("Please add to existing one "),
                          Text(" or "),
                          Text("create new group and add new members"),
                          Container(
                            padding: EdgeInsets.only(top: 50),
                            height: 200,
                            child:Image(
                              image: AssetImage("assets/images/sad.png"),
                            ), 
                            
                          )
                          
                          
                        ] 
                      ):
                      ListView.builder(
                      itemCount: friend.length+2,
                      itemBuilder: (context,index){
                        if(index==0){
                          return  MyMark(textval:"You have ${mydata["score"]} points " ,mark: (double.parse(mydata["score"])/100*5));
                        } 
                        if(index==friend.length+1){
                          return (
                            SizedBox(height: 60,)
                          );
                        }
                        return Card(
                          child: ListTile(
                              // selected: friend[index-1]["name"]=="Piumika"?true:false,
                              title: Text(friend[index-1]["name"]),
                              // trailing: Text(friend[index-1]["score"].toString()),
                              subtitle: Column(
                                children: <Widget>[
                                  FriendMark(mark:(double.parse(friend[index-1]["score"])/100*5)),
                                  Text(
                                    "${friend[index-1]["score"]}/100",
                                    style: TextStyle(
                                      color: Colors.red,
                                      fontSize:20,
                                    ),
                                  )
                                ]
                                ),
                                
                              trailing:Image(image:AssetImage('assets/images/badges/${friend[index-1]["badge"]}.jpg')),
                              onTap:(){},
                              leading:CircleAvatar(
                                
                                backgroundImage: AssetImage(
                                  friend[index-1]["gender"]=='M'?
                                  commoneVar.avatarMale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString():
                                  commoneVar.avatarFemale[
                                  (int.parse(friend[index-1]['avatar'])+1).toString()].toString()
                                ),
                              ),
                                    
                            )
                        );
                      }
                      ),
                  ],
                  ),
                  
        ),
      );
  }
}