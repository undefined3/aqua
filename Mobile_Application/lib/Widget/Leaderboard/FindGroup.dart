import 'package:aqua/Services/comonvar.dart';
import 'package:aqua/Services/leaderboardService.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';



class FindGroup extends StatefulWidget {
  FindGroup({Key key}) : super(key: key);

  @override
  _nameSFindGroup createState() => _nameSFindGroup();
}

class _nameSFindGroup extends State<FindGroup> {
  CommoneVar commoneVar=new CommoneVar();
  final weight = TextEditingController();
  LeaderBoardService leaderBoardService=new LeaderBoardService();
  List groups;
  List searchResult=[];

  bool spinner=true;

  @override
  void initState() { 
    super.initState();
    getGroups();
  }
  getGroups()async{
    List temp=await leaderBoardService.getGroup();
    setState(() {
      groups=temp;
      spinner=false;
    });
    setState(() {
      searchResult=temp;
    });
  }
  
  sendRequest(index)async{
    print(index);
    List temp=searchResult;
    temp[index]['spinner']=1;
    setState((){
      searchResult=temp;
    });
    bool result=await leaderBoardService.sendGroupRequest(searchResult[index]['id']);
    if(result){
      List temp=searchResult;
      temp[index]['spinner']=2;
      setState((){
        searchResult=temp;
      });
    }
    print(result);
  }

  setSearchResult(pattern){
    List temp=[];
    setState(() {
      spinner=true;
    });
  
    pattern=pattern.toString().toLowerCase();
    groups.forEach((element){
      String name=element['groupName'].toString().toLowerCase();   
      if(name.contains(pattern)){
        temp.add(element);
      }
    });
    setState(() {
      searchResult=temp;
      spinner=false;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      padding: EdgeInsets.fromLTRB(10, 50, 10, 20),
      color: Colors.white,
      child: Column(
        children: <Widget>[
          new TextFormField(
            onChanged: (value) => setSearchResult(value),
            decoration: new InputDecoration(
              suffixIcon:Icon(Icons.search),
              labelText: "Search Group",
              enabledBorder: const OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(20.0)),
                borderSide: const BorderSide(
                  color: Colors.grey,
                ),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(10.0)),
                borderSide: BorderSide(color: Colors.blue),
              ),
            ),
          ),
          spinner?Expanded(
            child:Center(
                child: Padding(
                    padding: EdgeInsets.only(top:100),
                    child:SpinKitWanderingCubes(
                        color: Colors.lightBlue,
                        size: 50.0
                    ),
                  )
            )
          )
          :
          groups.length==0?
          Text("No group Found"):
          Expanded(
            child:ListView.builder(
            itemCount: searchResult.length,
            itemBuilder: (context,index){
              return  
              Card(
                  margin: EdgeInsets.fromLTRB(10, 30, 0, 0),
                  child: ListTile(
                      title: Text(searchResult[index]['groupName'].toString()),
                      trailing:searchResult[index]['spinner']==1?
                        SizedBox(
                          child: SpinKitCircle(
                            color: Colors.lightBlue,
                            size: 30.0
                          ),
                          width: 30,
                        ):searchResult[index]['spinner']==0? 
                        IconButton(
                          icon: Icon(Icons.add_circle_outline),
                          color: Colors.blue,
                          onPressed: () => sendRequest(index)
                        ):
                        Icon(Icons.done,color: Colors.green),
                      subtitle: Text("${searchResult[index]['members']} members\n${searchResult[index]['admin']} is Admin"),
                      onTap:(){},
                      leading:CircleAvatar(
                        // backgroundColor: Colors.red,
                        // backgroundImage: NetworkImage(userAvatarUrl),
                        backgroundImage: NetworkImage(commoneVar.getUrl()+"getfile/groups/"+searchResult[index]["image"].toString()),
                      ),    
                    )
                );
            }
          ) ,
          )
          
        ],),
    );
  }
}