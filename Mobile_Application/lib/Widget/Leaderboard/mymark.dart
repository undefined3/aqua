import 'package:flutter/material.dart';
import 'package:flutter_rating_bar/flutter_rating_bar.dart';

class MyMark extends StatefulWidget {
  MyMark({Key key,this.textval,this.mark}) : super(key: key);
  final String textval;
  final double mark;
  @override
  _MyMarkState createState() => _MyMarkState();
}

class _MyMarkState extends State<MyMark> {
  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.fromLTRB(10, 30, 10, 20),
      color: Colors.white,
      shape: RoundedRectangleBorder(
                borderRadius: BorderRadius.circular(30.0),
                side: BorderSide(color: Colors.green, width: 1),
              ),
      child:Container(
      height: 200,
      // color: Colors.white,
      child:Padding(
        
        padding: EdgeInsets.fromLTRB(0, 30, 0, 0),
        child:Column(
          children:
            <Widget>[
              Text(widget.textval,style: TextStyle(fontSize: 25,color: Colors.blue[900])),
              SizedBox(height :30,),
              RatingBar(
                initialRating: widget.mark,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.blue[900],
                ),
                onRatingUpdate: (rating) {
                  print(rating);
                },
              ),
            ],
        ),
      )
    ) );
  }
}
class FriendMark extends StatefulWidget {
  // ({Key key,this.mark} : super(key: key);
  FriendMark({Key key,this.mark}) : super(key: key);
  final double mark;


  @override
  _FriendMarkState createState() => _FriendMarkState();
}

class _FriendMarkState extends State<FriendMark> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: RatingBar(
                
                initialRating: widget.mark,
                minRating: 1,
                direction: Axis.horizontal,
                allowHalfRating: true,
                itemCount: 5,
                itemPadding: EdgeInsets.symmetric(horizontal: 4.0),
                itemBuilder: (context, _) => Icon(
                  Icons.star,
                  color: Colors.blue[900],
                ),
                // onRatingUpdate: (rating) {
                //   print(rating);
                // },
              ),
    );
  }
}