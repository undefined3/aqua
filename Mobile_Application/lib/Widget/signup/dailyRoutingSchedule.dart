import 'package:aqua/Widget/signup/widget_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';

import 'package:flutter_time_picker_spinner/flutter_time_picker_spinner.dart';
import 'card_title.dart';
import 'package:intl/intl.dart';

DateTime now=DateTime.now();
DateTime _dateTimeBreakfast = new DateTime(now.year,now.month,now.day,8);
DateTime _dateTimeLunch = new DateTime(now.year,now.month,now.day,13);
DateTime _dateTimeDinner = new DateTime(now.year,now.month,now.day,20);
DateTime _dateTimeWakeup = new DateTime(now.year,now.month,now.day,6);
DateTime _dateTimeSleep = new DateTime(now.year,now.month,now.day,22);
Map formData;
class DailyRoutingTime extends StatelessWidget {
  Widget build(BuildContext context) {
    formData = ModalRoute.of(context).settings.arguments;
  
    return Scaffold(
      body: Padding(
        padding: MediaQuery.of(context).padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildTitle(context),
            Expanded(child: _buildCards(context)),
            _buildBottom(context),
          ],
        ),
      ),
    );
  }
  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 24.0,
        top: screenAwareSize(56.0, context),
      ),
      child: Text(
        "Daily Routing Schedule",
        style: new TextStyle(fontSize: 28.0, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildCards(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 14.0,
        right: 14.0,
        top: screenAwareSize(32.0, context),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[
                Expanded(child:    Card(
                   child: SizedBox(
                     width: double.infinity,
                       child: Padding(
                           padding: EdgeInsets.only(top: screenAwareSize(8.0, context)),
                           child: Column(
                             crossAxisAlignment: CrossAxisAlignment.center,
                             children: <Widget>[
                                CardTitle("Breakfast time"),
                                  Expanded(child: Card(child: DigiClock(tag:1,time:_dateTimeBreakfast)))
            ],
          ),
        ),
      ),
    )),
                Expanded(child:    Card(
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.only(top: screenAwareSize(8.0, context)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CardTitle("Lunch  time"),
                          Expanded(child: Card(child: DigiClock(tag:2,time:_dateTimeLunch)))
                        ],
                      ),
                    ),
                  ),
                )),
                Expanded(child:    Card(
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.only(top: screenAwareSize(8.0, context)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CardTitle("Dinner time"),
                          Expanded(child: Card(child: DigiClock(tag:3,time:_dateTimeDinner)))
                        ],
                      ),
                    ),
                  ),
                )),
              ],
            ),

          ),
          Expanded(
            child: Column(
              children: <Widget>[
                Expanded(child:    Card(
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.only(top: screenAwareSize(8.0, context)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CardTitle("Wakeup time"),
                          Expanded(child: Card(child: DigiClock(tag:4,time:_dateTimeWakeup)))
                        ],
                      ),
                    ),
                  ),
                )),
                Expanded(child:    Card(
                  child: SizedBox(
                    width: double.infinity,
                    child: Padding(
                      padding: EdgeInsets.only(top: screenAwareSize(8.0, context)),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        children: <Widget>[
                          CardTitle("Sleep time"),
                          Expanded(child: Card(child: DigiClock(tag:5,time:_dateTimeSleep)))
                        ],
                      ),
                    ),
                  ),
                )),
              ],
            ),
          ),

        ],
      ),
    );
  }

  Widget _tempCard(String label){
    return Card(
      child: Container(
        width: double.infinity,
        height: double.infinity,
        child: Text(label),
      ),
    );
  }

  Widget _buildBottom(BuildContext context) {
    return Container(
        child: ListTile(
          title: Row(
            children: <Widget>[
              Expanded(child: RaisedButton(onPressed: () {

                Navigator.pop(context);

                },child: Text("Back"),color: Colors.blueGrey,textColor: Colors.white,)),
              Expanded(
                child:Builder(builder: (context) =>
                  RaisedButton(onPressed: () {
                    if(_dateTimeWakeup.compareTo(_dateTimeBreakfast)<0){
                      if(_dateTimeBreakfast.compareTo(_dateTimeLunch)<0){
                        if(_dateTimeLunch.compareTo(_dateTimeDinner)<0){
                          if(_dateTimeDinner.compareTo(_dateTimeSleep)<0){
                             
                          }else{
                            Scaffold.of(context).showSnackBar(
                              SnackBar(
                                content:ListTile(
                                  title: Text("Please select valid schedule"),
                                  trailing:Icon(Icons.assignment_late)
                                ) ,
                              backgroundColor: Colors.red,
                              )
                            );
                            return;
                          }
                        }else{
                          Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Please select valid schedule"),
                                    trailing:Icon(Icons.assignment_late)
                                  ) ,
                                backgroundColor: Colors.red,
                                )
                              );
                              return;
                        }
                      }else{
                        Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Please select valid schedule"),
                                    trailing:Icon(Icons.assignment_late)
                                  ) ,
                                backgroundColor: Colors.red,
                                )
                              );
                              return;
                      }
                    }else{
                      Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Please select valid schedule"),
                                    trailing:Icon(Icons.assignment_late)
                                  ) ,
                                backgroundColor: Colors.red,
                                )
                              );
                              return;
                    }
                    
                    // print(temp);
                    DateFormat.jm().format(_dateTimeBreakfast);
                    formData["breakfastTime"]=DateFormat.jm().format(_dateTimeBreakfast);
                    formData["lunchTime"]=DateFormat.jm().format(_dateTimeLunch);
                    formData["dinnerTime"]=DateFormat.jm().format(_dateTimeDinner);
                    formData["wakupTime"]=DateFormat.jm().format(_dateTimeWakeup);
                    formData["sleepTime"]=DateFormat.jm().format(_dateTimeSleep);
                    Navigator.pushNamed(context , "/register/4",arguments: formData);
                },
                  child: Text("Next"),color: Colors.blueGrey,textColor: Colors.white,)),

                ), 
              
            ],
          ),
        )
    );
  }

}

class DigiClock extends StatefulWidget {
  DigiClock({Key key,this.tag,this.time}) : super(key: key);
  final int tag;
  final DateTime time;
  @override
  _DigiClockState createState() => _DigiClockState();
}

class _DigiClockState extends State<DigiClock> {
  @override
  // void initState() {
  //   // TODO: implement initState
  //   super.initState();
  //   _dateTimeBreakfast.
  // }

  @override
  Widget build(BuildContext context) {
    return new TimePickerSpinner(
      is24HourMode: false,
      time: widget.time,
      normalTextStyle: TextStyle(
          fontSize: 15,
          color: Colors.lightBlueAccent
      ),
      highlightedTextStyle: TextStyle(
          fontSize: 24,
          color: Colors.blueAccent
      ),
      spacing: 3,
      itemHeight: 40,
      isForce2Digits: true,
      onTimeChange: (time) {
        if(widget.tag==1){
          setState(() {
            _dateTimeBreakfast = time;
          });
        }else if(widget.tag==2){
          setState(() {
            _dateTimeLunch = time;
          });
        }else if(widget.tag==3){
          setState(() {
            _dateTimeDinner = time;
          });
        }else if(widget.tag==4){
          setState(() {
            _dateTimeWakeup = time;
          });
        }else if(widget.tag==5){
          setState(() {
            _dateTimeSleep = time;
          });
        }
        
        
      },
    );
  }
}
