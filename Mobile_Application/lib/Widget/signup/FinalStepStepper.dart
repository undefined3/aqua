import 'package:aqua/Widget/signup/widget_utils.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:intl/intl.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import '../../Services/userservice.dart';
import 'package:cupertino_date_textbox/cupertino_date_textbox.dart';
const appName = 'DateTimeField Example';



Map formData;
bool load=false;
dynamic activityLevel=null;
Future<bool> register()async{
  // formData['activityLevel']=activityLevel.toString();
  UserService service =UserService();
  dynamic result=await service.registration(formData);
  print(result['status']);
  if(result['status']==true){
    return true;
  }else{
    return false;
  }

}
class FinalStep extends StatelessWidget {
  
  Widget build(BuildContext context) {
    formData = ModalRoute.of(context).settings.arguments;
    return Scaffold(
      body: Padding(
        padding: MediaQuery
            .of(
            context)
            .padding,
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            _buildTitle(
                context),
            Expanded(
                child: _buildCards(
                    context)),
            _buildBottom(
                context),
          ],
        ),
      ),
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 24.0,
        top: screenAwareSize(
            56.0, context),
      ),
      child: Text(
        "Other Details",
        style: new TextStyle(
            fontSize: 28.0, fontWeight: FontWeight.bold),
      ),
    );
  }


  Widget _buildCards(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 14.0,
        right: 14.0,
        top: screenAwareSize(
            32.0, context),
      ),
      child: Row(
        children: <Widget>[
          Expanded(
            child: Column(
              children: <Widget>[

                Expanded(
                    child: MyHome()),
                // Expanded(
                  // child:
                  // ActivityLevel(),
                  // SizedBox(height: 70,),
                // ),    
                Expanded(
                    child: RadioGroup()),
              ],
            ),

          ),


        ],
      ),
    );
  }



//  Widget _tempCard() {
//    return Card(
//     child: Container(
//    child: Column(
//    children: <Widget>[
//
//      InkWell(
//        onTap: () {
//          BasicDateField();   // Call Function that has showDatePicker()
//        },
//        child: IgnorePointer(
//          child: new TextFormField(
//            decoration: new InputDecoration(hintText: 'Enter Your Birthday'),
//
//            // validator: validateDob,
//            onSaved: (String val) {},
//          ),
//        ),
//      ),
//    ],
//    )
//    )
//    );
//  }

}
class ActivityLevel extends StatefulWidget {
  ActivityLevel({Key key}) : super(key: key);

  @override
  _ActivityLevelState createState() => _ActivityLevelState();
}

class _ActivityLevelState extends State<ActivityLevel> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: DropdownButton<String>(
                    underline: Container(
                      height: 0,
                      color: Colors.deepPurpleAccent,
                    ),
                    // itemHeight: 20,
                    
                    hint: Text("Select Your Activity Level"),
                    value: activityLevel,
                    items: <String>['Low Active Level','High Activity Level', 'Normal Activity Level'].map((String value) {
                      return new DropdownMenuItem<String>(
                        value: value,
                        child: new Text(value),
                      );
                    }).toList(),
                    onChanged: (value) {
                      setState(() {
                        activityLevel=value;
                       });
                    },
                  )
    );
  }
}


class MyHome extends StatefulWidget {
  @override
  _MyHomeState createState() => new _MyHomeState();
}

class _MyHomeState extends State<MyHome> {
  DateTime _selectedDateTime = DateTime.now();

  @override
  void initState() {

    super.initState();
    formData['birthday']=DateTime.now().toString();
  }

  @override
  Widget build(BuildContext context) {
    final String formattedDate = DateFormat.yMd().format(_selectedDateTime);
    final selectedText = Text('You selected: $formattedDate');

    final birthdayTile = new Material(
      color: Colors.transparent,
      child: new Column(
        crossAxisAlignment: CrossAxisAlignment.start,
        children: <Widget>[
          const Text('Enter Your Birthday',
              style: TextStyle(
                color: CupertinoColors.systemBlue,
                fontSize: 15.0,
              )),
          const Padding(
            padding: EdgeInsets.only(bottom: 5.0),
          ),
          CupertinoDateTextBox(
              initialValue: _selectedDateTime,
              onDateChange: onBirthdayChange,
              hintText: DateFormat.yMd().format(_selectedDateTime)),
        ],
      ),
    );

    return new Scaffold(
      body: Padding(
          padding: const EdgeInsets.fromLTRB(20, 5, 20, 0),
          child: Column(children: <Widget>[
            selectedText,
            const SizedBox(height: 5.0),
            birthdayTile
          ])),
    );
  }

  void onBirthdayChange(DateTime birthday) {
    setState(() {
      _selectedDateTime = birthday;
    });
    formData['birthday']=birthday.toString();
  }
}




Widget _buildBottom(BuildContext context) {
  return Container(
      child: ListTile(
        
        title:Row(
          children: <Widget>[
            Expanded(child: RaisedButton(onPressed: () {
              Navigator.pop(context);
            },child: Text("Back"),color: Colors.blueGrey,textColor: Colors.white,)),
            Expanded(child: RaisedButton(onPressed: ()async{
              // showDialog(context: null)
              showDialog(
               context:context,
               barrierDismissible:false,
               builder: (BuildContext context)=>AlertDialog(
                 title:Column(
                   children: <Widget>[
                     Text("Registering"),
                     SizedBox(height: 30,),
                     SpinKitCircle(color: Colors.blue,size: 50.0,)
                   ],)
               )
              );
              dynamic result=await register();
              if(result){
                Navigator.of(context).pushNamedAndRemoveUntil('/', ModalRoute.withName('/'));
              }else{
                  Navigator.of(context).pop();
              }
              

            },child:Text("Register"),color: Colors.blueGrey,textColor: Colors.white,)),
          ],
        ),
      )
  );
}

class RadioGroup extends StatefulWidget {
  @override
  RadioGroupWidget createState() => RadioGroupWidget();
}

class FruitsList {
  String name;
  int index;
  FruitsList({this.name, this.index});
}

class RadioGroupWidget extends State <RadioGroup>{

@override
void initState() { 
  super.initState();
  formData['CKD']='0';
}
  // Default Radio Button Item
 String radioItem = '';

  // Group Value for Radio Button.
  int id = 1;

  List<FruitsList> fList = [
    FruitsList(
      index: 1,
      name: "No",
    ),
    FruitsList(
      index: 2,
      name: "Yes",
    ),
  ];

  Widget build(BuildContext context) {
    return Column(

      children: <Widget>[
        Text("Do you have Chronic Kidney Disease(CKD)?" , textAlign: TextAlign.left,style: TextStyle(fontSize: 15)),
        Padding(
            padding : EdgeInsets.all(5.0),
            child: Text('$radioItem', style: TextStyle(fontSize: 15))
        ),


        Expanded(
            child: Container(
              //height: 350.0,
              child: Column(

                children:

                fList.map((data) => RadioListTile(
                  title: Text("${data.name}"),
                  groupValue: id,
                  value: data.index,
                  onChanged: (val) {
                    setState(() {
                  //    radioItem = data.name ;
                      id = data.index;
                    });
                    formData['CKD']=data.index==1?'0':'1';
                  },
                )).toList(),
              ),
            )),

      ],
    );
  }
}