import 'package:flutter/material.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';




enum AuthMode { LOGIN, SINGUP }

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: SignupPage(),
    );
  }
}

class SignupPage extends StatefulWidget {
  @override
  _SignupPageState createState() => _SignupPageState();
}

class _SignupPageState extends State<SignupPage> {
  // To adjust the layout according to the screen size
  // so that our layout remains responsive ,we need to
  // calculate the screen height
  double screenHeight;
  UserService service;

  // Set intial mode to login
  AuthMode _authMode = AuthMode.LOGIN;
  final name = TextEditingController();
  final email = TextEditingController();
  final password = TextEditingController();
  final repassword = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool load=false;
  bool emailError=false;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    service=UserService();
  }
  @override
  Widget build(BuildContext context) {
    screenHeight = MediaQuery.of(context).size.height;

    return Scaffold(
      body: Container(
        constraints: BoxConstraints.expand(),
        
        // decoration: BoxDecoration(
        //     image: DecorationImage(
        //       image: AssetImage('assets/images/background.png'),
        //       fit: BoxFit.cover,
        //     )),
        child: SingleChildScrollView(
          child: Stack(
            children: <Widget>[
              //lowerHalf(context),
              //upperHalf(context),
              _authMode == AuthMode.LOGIN
                  ? singUpCard(context)
                  : singUpCard(context),
              pageTitle(),
            ],
          ),
        ),
      ),
    );
  }

  Widget pageTitle() {
    return Center(
      child:Container(
      margin: EdgeInsets.only(top: 50),
      child: Image(image: AssetImage('assets/images/Logo/new2.png'),
      height: 80,)
      // Row(
      //   crossAxisAlignment: CrossAxisAlignment.center,
      //   mainAxisAlignment: MainAxisAlignment.center,
      //   children: <Widget>[
      //     Icon(
      //       Icons.,
      //       size: 48,
      //       color: Colors.white,
      //     ),
      //     Text(
      //       "AQUA",
      //       style: TextStyle(
      //           fontSize: 34, color: Colors.white, fontWeight: FontWeight.w400),
      //     )
      //   ],
      // ),
    ) );
  }

//  Widget loginCard(BuildContext context) {
//    return Column(
//      children: <Widget>[
//        Container(
//          margin: EdgeInsets.only(top: screenHeight / 4),
//          padding: EdgeInsets.only(left: 10, right: 10),
//          child: Card(
//            shape: RoundedRectangleBorder(
//              borderRadius: BorderRadius.circular(10),
//            ),
//            elevation: 8,
//            child: Padding(
//              padding: const EdgeInsets.all(30.0),
//              child: Column(
//                crossAxisAlignment: CrossAxisAlignment.center,
//                children: <Widget>[
//                  Align(
//                    alignment: Alignment.topLeft,
//                    child: Text(
//                      "Login",
//                      style: TextStyle(
//                        color: Colors.black,
//                        fontSize: 28,
//                        fontWeight: FontWeight.w600,
//                      ),
//                    ),
//                  ),
//                  SizedBox(
//                    height: 15,
//                  ),
//                  TextFormField(
//                    decoration: InputDecoration(
//                        labelText: "Your Email or Username", hasFloatingPlaceholder: true),
//                  ),
//                  SizedBox(
//                    height: 20,
//                  ),
//                  TextFormField(
//                    decoration: InputDecoration(
//                        labelText: "Password", hasFloatingPlaceholder: true),
//                  ),
//                  SizedBox(
//                    height: 20,
//                  ),
//                  Row(
//                    mainAxisAlignment: MainAxisAlignment.spaceEvenly,
//                    children: <Widget>[
//                      MaterialButton(
//                        onPressed: () {},
//                        child: Text("Forgot Password ?"),
//                      ),
//                      Expanded(
//                        child: Container(),
//                      ),
//                      FlatButton(
//                        child: Text("Login"),
//                        color: Color(0xFF4B9DFE),
//                        textColor: Colors.white,
//                        padding: EdgeInsets.only(
//                            left: 38, right: 38, top: 15, bottom: 15),
//                        shape: RoundedRectangleBorder(
//                            borderRadius: BorderRadius.circular(5)),
//                        onPressed: () {},
//                      )
//                    ],
//                  )
//                ],
//              ),
//            ),
//          ),
//        ),
//        Row(
//          crossAxisAlignment: CrossAxisAlignment.center,
//          mainAxisAlignment: MainAxisAlignment.center,
//          children: <Widget>[
//            SizedBox(
//              height: 40,
//            ),
//            Text(
//              "Don't have an account ?",
//              style: TextStyle(color: Colors.grey),
//            ),
//            FlatButton(
//              onPressed: () {
//                setState(() {
//                  _authMode = AuthMode.SINGUP;
//                });
//              },
//              textColor: Colors.black87,
//              child: Text("Create Account"),
//            )
//          ],
//        )
//      ],
//    );
//  }

  Widget singUpCard(BuildContext context) {
    return Column(
      children: <Widget>[
        Container(
          margin: EdgeInsets.only(top: screenHeight / 5),
          padding: EdgeInsets.only(left: 10, right: 10),
          child: Card(
            shape: RoundedRectangleBorder(
              borderRadius: BorderRadius.circular(10),
            ),
            elevation: 8,
            child: Padding(
              padding: const EdgeInsets.all(30.0),
              child: Form(
                key: _formKey,
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.center,
                  children: <Widget>[
                    Align(
                      alignment: Alignment.topLeft,
                      child: Text(
                        "Create Account",
                        style: TextStyle(
                          color: Color.fromRGBO(102, 224, 255, 1),
                          fontSize: 28,
                          fontWeight: FontWeight.w600,
                        ),
                      ),
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter your Name';
                          }
                          return null;
                      },
                      controller: name,
                      decoration: InputDecoration(
                          labelText: "Full Name ",
                          hasFloatingPlaceholder: true,
                          fillColor: Colors.red,
                      ),
                    
                    ),
                    SizedBox(
                      height: 15,
                    ),
                    TextFormField(
                      validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter your Email';
                          }else{
                            String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                            RegExp regExp = new RegExp(p);
                            return regExp.hasMatch(value)?null:"Please enter valid email";
                          }
                      },
                      controller: email,
                      decoration: InputDecoration(
                          labelText: "Your Email", hasFloatingPlaceholder: true),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    emailError?SizedBox(
                      child:Text("This email has already used",style: TextStyle(color: Colors.red),) ,):
                    SizedBox(height: 1,)
                    ,
                    TextFormField(
                      obscureText:true,
                      validator: (value) {
                          if (value.isEmpty) {
                            return 'Please enter password';
                          }else{
                            String  pattern = r'^(?=.*?[A-Z])(?=.*?[a-z])(?=.*?[0-9])(?=.*?[!@#\$&*~]).{8,}$';
                            RegExp regExp = new RegExp(pattern);
                            return regExp.hasMatch(value)?null:"Invalid Password";
                          }
                      },
                      controller: password,
                      decoration: InputDecoration(
                          labelText: "Password", hasFloatingPlaceholder: true),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    TextFormField(
                      validator: (value) {
                          if (value.isEmpty) {
                            return 'Enter your Password again';
                          }else if(value.toString()!=password.text){
                            return "Password not match";
                          }
                          return null;
                      },
                      obscureText:true,
                      controller: repassword,
                      decoration: InputDecoration(
                          labelText: "Re-type Password", hasFloatingPlaceholder: true),
                    ),
                    SizedBox(
                      height: 20,
                    ),
                    Text(
                      "Password must be at least 8 characters including at least one uppercase letter, one numeric digit, and one special character",
                      style: TextStyle(color: Colors.grey),
                    ),
                    SizedBox(
                      height: 5,
                    ),
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                      children: <Widget>[
                        Expanded(
                          child: Container(),
                        ),
                        FlatButton(
                          child: load?SpinKitThreeBounce(color: Colors.white,size: 20.0,):Text("Sign Up"),
                          color: Color.fromRGBO(102, 224, 255, 1),
                          // color: Color(0xFF4B9DFE),
                          textColor: Colors.white,
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 15, bottom: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () async{
                           if(_formKey.currentState.validate()){
                              // Route route = MaterialPageRoute(builder: (context) => InputPage());
                              // Navigator.of(context).pushNamed('/stepper');
                              setState(() {
                                load=true;
                              });
                              dynamic temp =await service.isExist(email.text);
                              if(temp==false){
                                var formData={
                                  "name":name.text,
                                  "email":email.text,
                                  "password":password.text
                                };
                                setState(() {
                                  load=false;
                                });
                                Navigator.pushNamed(context ,"/register/2",arguments:formData);
                              }else{
                                setState(() {
                                  emailError=true;
                                });
                                setState(() {
                                  load=false;
                                });
                              }
                              

                           }
                          },
                        ),
                      ],
                    ),
                  ],
                ),
              ),
            ),
          ),
        ),
        Row(
          crossAxisAlignment: CrossAxisAlignment.center,
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            SizedBox(
              height: 40,
            ),
            Text(
              "Already have an account?",
              style: TextStyle(color: Colors.grey),
            ),
            FlatButton(
              onPressed: () {
                 Navigator.pushNamed(context, '/login');
                // setState(() {
                //   _authMode = AuthMode.LOGIN;
                // });
              },
              textColor: Colors.black87,
              child: Text("Login"),
            )
          ],
        ),
        Align(
          alignment: Alignment.bottomCenter,
          child: FlatButton(
            child: Text(
              "Terms & Conditions",
              style: TextStyle(
                color: Colors.grey,
              ),
            ),
            onPressed: () {},
          ),
        ),
      ],
    );
  }

//  Widget upperHalf(BuildContext context) {
//    return Container(
//      height: screenHeight / 2,
//
//      child: Image.asset(
//        'images/bg4.jpg',
//        fit: BoxFit.cover,
//
//      ),
//    );
//  }

//  Widget lowerHalf(BuildContext context) {
//    return Align(
//      alignment: Alignment.bottomCenter,
//      child: Container(
//        height: screenHeight / 2,
//        color: Color(0xFFECF0F3),
//      ),
//    );
//  }
}
