import 'package:aqua/Widget/signup/weight_card.dart';
import 'package:flutter/material.dart';
import 'gender.dart';
import 'gender_card.dart';
import 'height_card.dart';
import 'widget_utils.dart' show screenAwareSize;

class InputPage extends StatefulWidget {
  @override
  InputPageState createState() {
    return new InputPageState();
  }
}

class InputPageState extends State<InputPage>{

  Gender gender = Gender.other;
  int height = 180;
  int weight = 70;
  Map formData={};

  @override
  Widget build(BuildContext context) {
    formData = ModalRoute.of(context).settings.arguments;
    print(formData);
    return Stack(
      children: <Widget>[
        Scaffold(

          body: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              _buildTitle(context),
              InputSummaryCard(
                gender: gender,
                weight: weight,
                height: height,
              ),

              Expanded(child: _buildCards(context)),
              _buildBottom(context),
            ],
          ),
        ),

      ],
    );
  }

  Widget _buildTitle(BuildContext context) {
    return Padding(
      padding: EdgeInsets.only(
        left: 24.0,
        top: screenAwareSize(56.0, context),
      ),
      child: Text(
        "Basic Details",
        style: new TextStyle(fontSize: 20.0, fontWeight: FontWeight.bold),
      ),
    );
  }

  Widget _buildCards(BuildContext context) {
    return Row(
      children: <Widget>[
        Expanded(
          child: Column(
            children: <Widget>[
              Expanded(
                child: GenderCard(
                  gender: gender,
                  onChanged: (val) => setState(() => gender = val),
                ),
              ),
              Expanded(
                child: WeightCard(
                  weight: weight,
                  onChanged: (val) => setState(() => weight = val),
                ),
              ),
            ],
          ),
        ),
        Expanded(
          child: HeightCard(
            height: height,
            onChanged: (val) => setState(() => height = val),
          ),
        )
      ],
    );
  }

  Widget _buildBottom(BuildContext context) {
    return Container(
        child: ListTile(
          title: Row(
            children: <Widget>[
//              Expanded(child: RaisedButton(onPressed: () {
//
//                },child: Text("Back"),color: Colors.blueGrey,textColor: Colors.white,)),
              Expanded(child: RaisedButton(onPressed: () {
                // Route route = MaterialPageRoute(builder: (context) => DailyRoutingTime());
                formData["weight"]=weight.toString();
                formData["height"]=height.toString();
                formData["gender"]=(gender==Gender.female?"F":gender==Gender.male?"M":"O");
                print(formData);
                Navigator.pushNamed(context ,'/register/3',arguments:formData);

              },child: Text("Next"),color: Colors.blueGrey,textColor: Colors.white,)),
            ],
          ),
        )
    );
  }



}

class InputSummaryCard extends StatelessWidget {
  final Gender gender;
  final int height;
  final int weight;

  const InputSummaryCard({Key key, this.gender, this.height, this.weight})
      : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Card(
      margin: EdgeInsets.all(screenAwareSize(15.0, context)),
      child: SizedBox(
        height: screenAwareSize(32.0, context),
        child: Row(
          children: <Widget>[
            Expanded(child: _genderText()),
            _divider(),
            Expanded(child: _text("${weight}kg")),
            _divider(),
            Expanded(child: _text("${height}cm")),

          ],
        ),
      ),
    );
  }

  Widget _genderText() {
    String genderText = gender == Gender.other
        ? '-'
        : (gender == Gender.male ? 'Male' : 'Female');
    return _text(genderText);
  }

  Widget _text(String text) {
    return Text(
      text,
      style: TextStyle(
        color: Color.fromRGBO(143, 144, 156, 1.0),
        fontSize: 15.0,
      ),
      textAlign: TextAlign.center,
    );
  }

  Widget _divider() {
    return Container(
      width: 1.0,
      color: Color.fromRGBO(151, 151, 151, 0.1),
    );
  }
}