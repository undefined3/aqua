import 'package:flutter/material.dart';

class DailyActivityLevel extends StatefulWidget {
  DailyActivityLevel({Key key}) : super(key: key);

  @override
  _DailyActivityLevelState createState() => _DailyActivityLevelState();
}

class _DailyActivityLevelState extends State<DailyActivityLevel> {
  Map formData;
  String val;
  
  @override
  Widget build(BuildContext context) {

    formData = ModalRoute.of(context).settings.arguments;
    return Container(
       child: Scaffold(
        
        body:SafeArea(
          child:ListView(
            children: <Widget>[

              Container(
                margin: EdgeInsets.only(top:30,bottom: 40),
                child:Center(
                  child:Text("Select Your Daily Activity Level",style: TextStyle(color: Colors.blue,fontSize: 25),)
                ) ,
              ),
              Card(
                child: ListTile(
                  leading:Radio(
                    groupValue:val,
                    value:"1",
                    focusColor: Colors.red,
                    onChanged: (value) => {
                      setState(() {
                        val=value;
                      })
                    },
                  ),  
                    title:Image(
                      // width: 100,
                      height: 150,
                      image: AssetImage('assets/farmer.jpg')
                    ),
                  ),
              ),
              Card(
                child: ListTile(
                  leading:Radio(
                    groupValue:val,
                    value:"2",
                    focusColor: Colors.red,
                    onChanged: (value) => {
                      setState(() {
                        val=value;
                      })
                    },
                  ),  
                    title:Image(
                      // width: 100,
                      height: 150,
                      image: AssetImage('assets/sportsman.jpg')
                    ),
                  ),
              ),
              Card(
                child: ListTile(
                  leading:Radio(
                    groupValue:val,
                    value:"3",
                    focusColor: Colors.red,
                    onChanged: (value) => {
                      setState(() {
                        val=value.toString();
                      })
                    },
                  ),  
                    title:Image(
                      // width: 10,
                      height: 150,
                      image: AssetImage('assets/officeworker.png')
                    ),
                  ),
              ),
              Card(
                child: ListTile(
                  leading:Radio(
                    groupValue:val,
                    value:"4",
                    focusColor: Colors.red,
                    onChanged: (value) => {
                      setState(() {
                        val=value;
                      })
                    },
                  ),  
                    title:Image(
                      // width: 100,
                      height: 150,
                      image: AssetImage('assets/indoorworker.jpg')
                    ),
                  ),
              ),

              Container(
                child: ListTile(
                  title: Row(
                    children: <Widget>[
                      Expanded(child: RaisedButton(onPressed: () {
                        Navigator.pop(context);
                        },child: Text("Back"),color: Colors.blueGrey,textColor: Colors.white,)),
                      Expanded(child: RaisedButton(onPressed: () {
                        // DateFormat.jm().format(_dateTimeBreakfast);
                        // formData["breakfastTime"]=DateFormat.jm().format(_dateTimeBreakfast);
                        // formData["lunchTime"]=DateFormat.jm().format(_dateTimeLunch);
                        // formData["dinnerTime"]=DateFormat.jm().format(_dateTimeDinner);
                        // formData["wakupTime"]=DateFormat.jm().format(_dateTimeWakeup);
                        print(val);
                        print("val");
                        formData["activityLevel"]=val.toString();
                        print(formData);
                        Navigator.pushNamed(context , "/register/5",arguments: formData);
                      },
                        child: Text("Next"),color: Colors.blueGrey,textColor: Colors.white,)),
                    ],
                  ),
                )
              )
              
              // Text("llll"),

            ]
          )
        )
         
       ),
    );
  }
}

