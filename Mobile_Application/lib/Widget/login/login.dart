//login pagee

import 'package:flutter/material.dart';
import '../../Services/userservice.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:shared_preferences/shared_preferences.dart';


class Login extends StatefulWidget {
  @override
  _LoginState createState() => _LoginState();
}


class _LoginState extends  State<Login> {
  Future<SharedPreferences> _prefs = SharedPreferences.getInstance();
  final email = TextEditingController();
  final password = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  bool load=false;
  bool error=false;
  UserService userService=UserService();

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    isLogedIn();
  }
  
  @override
  void dispose() {
    email.dispose();
    password.dispose();
    super.dispose();
    
  }
  void isLogedIn() async{    
    dynamic status=await userService.isLogedIn();
    if(status==true){
      Navigator.of(context).pushNamedAndRemoveUntil('/dasboard', ModalRoute.withName('/'));
    }
  }
  @override
  Widget build(BuildContext context) {


    Future<void> loginFunction() async{
      dynamic result=await userService.login(email.text, password.text);
      
      if(result['status']==true){
          print(result['jwt']);
          setState(() {
            error=false;
          });
          setState(() {
          load=false;
          });
        Navigator.of(context).pushNamedAndRemoveUntil('/dasboard', ModalRoute.withName('/'));
      }else{
        setState(() {
          load=false;
        });
        setState(() {
          error=true;
        });
      }
      
      // print(result.toString()+"123");
    }
    return Scaffold(
      backgroundColor: Colors.white,
      body: Container(
        child: SingleChildScrollView(
            child: Column(
            children: <Widget>[
              //Background Image
              Container(
                height: 350,
                width: 600,
                decoration: BoxDecoration(
                  
                  image: DecorationImage(
                    image: AssetImage('assets/images/loginbg.jpg'),
                    fit: BoxFit.fitWidth
                  ),
                ),
                //Login Button
                child: Stack(
                  children: <Widget>[
              Positioned(
                child: Container(
                  margin: EdgeInsets.only(top: 30),
                  child: Center(
                    child: Text("Login", style: TextStyle(color: Colors.white, fontSize: 40, fontWeight: FontWeight.bold),),
                  ),
                )
              ),
                  ],
                ),
              ),
              //Containers
              Padding(
                padding: EdgeInsets.all(30.0),
                child: Column(
                  children: <Widget>[
                    Container(
                      padding: EdgeInsets.all(5),
                      decoration: BoxDecoration(
                        color: Colors.white,
                        borderRadius: BorderRadius.circular(10),
                        boxShadow: [
                          BoxShadow(
                            color: Color.fromRGBO(143, 148, 251, .6),
                            blurRadius: 20.0,
                            offset: Offset(0,10)
                          )
                        ]
                      ),
                      child: Form(
                        key: _formKey,
                        child:Column(
                          children: <Widget>[
                            Container(
                              padding: EdgeInsets.all(8.0),
                              decoration: BoxDecoration(
                                border: Border(bottom: BorderSide(color: Colors.grey[100]))
                              ),
                              child: TextFormField(
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter your email';
                                  }else{
                                    String p = r'^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$';
                                    RegExp regExp = new RegExp(p);
                                    return regExp.hasMatch(value)?null:"Please enter valid email";
                                  }
                                },
                                controller: email,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Email",
                                  hintStyle: TextStyle(color: Colors.grey[700])
                                )
                              ),
                            ),
                            Container(
                              padding: EdgeInsets.all(8.0),
                              child: TextFormField(
                                obscureText:true,
                                validator: (value) {
                                  if (value.isEmpty) {
                                    return 'Please enter your password';
                                  }
                                  return null;
                                },
                                controller: password,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                  hintText: "Password",
                                  hintStyle: TextStyle(color: Colors.grey[700])
                                ),
                              ),
                            )
                          ]
                        ),
                      ),
                    ),
                    //Login button
                    SizedBox(height: 30,),
                    error?Text("Invalid email or Password",style: TextStyle(color: Colors.red),):SizedBox(height: 1,),
                    error?SizedBox(height: 30,):SizedBox(height: 1,),
                    Container(
                      height: 50,
                      decoration: BoxDecoration(
                        borderRadius: BorderRadius.circular(10),
                        gradient: LinearGradient(
                          colors: [
                            Color.fromRGBO(102, 224, 255, 1),
                            Color.fromRGBO(102, 224, 255, 1)
                          ]
                        ) 
                      ),
                      child: Padding(
                        padding: const EdgeInsets.all(1.0),
                        child:Column(
                          children: <Widget>[
                            FlatButton(onPressed: ()async {

                              setState(() {
                                error=false;
                              });
                              if (_formKey.currentState.validate()) {
                                setState(() {
                                  load=true;
                                });
                                // If the form is valid, display a Snackbar.
                                await loginFunction();
                                
                              }
                              
                            },
                      child: Center(
                        child:load?
                          SpinKitThreeBounce(color: Colors.white,size: 45.0,):
                          Text("Login", style: TextStyle(color: Colors.white,fontSize: 20, fontWeight: FontWeight.bold),
                        ),
                      ),
                            ),
                          ]
                            ),
                      ),
                    ),
                    SizedBox(height: 20,),
                    // Text("Forgot Password?", style: TextStyle(color: Colors.black),),
                    SizedBox(height:20),
                    Row(
                      
                      children: <Widget>[
                      SizedBox(width: 40.0,),
                      Text(
                        "Don't have an account?",
                        style: TextStyle(color: Colors.grey),
                      ),
                      FlatButton(
                        onPressed: () {
                        Navigator.pushNamed(context, '/register/1');
                        },
                        textColor: Colors.black87,
                        child: Text(" SignUp"),
                      )
                    ],)
                    
                  ],
                ),
                )
            ],
          ),
        ),
      ),
      );
  }
}