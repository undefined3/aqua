import 'package:flutter/material.dart';

class Startup extends StatefulWidget {
  @override
  _StartupState createState() => _StartupState();
}

class _StartupState extends State<Startup> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
     // background image
     backgroundColor: Colors.white,       
     body: Container(
        child: Column(
          children: <Widget>[
            Container(
              height: 350,
              width: 600,
              decoration: BoxDecoration(
                image: DecorationImage(
                  image: AssetImage('assets/images/loginbg.jpg'),
                  fit: BoxFit.cover
                ),
              ), 
              child: Stack(
                children: <Widget>[
                  // Positioned(
                  //   width: 200,
                  //   height: 150,
                  //   child: Container(
                  //     decoration: BoxDecoration(
                  //       image: DecorationImage(
                  //         image: AssetImage('assets/images/logo.jpeg')
                  //       )
                  //     ),  
                  //   ),
                  // )
                ],
              ) 
            ),
            //facebook button
          SizedBox(height: 30,),
            Container(
              height: 50,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
              colors: [
                Color.fromRGBO(102, 224, 255, 1),
                Color.fromRGBO(102, 224, 255, 1)
                // Color.fromRGBO(143, 148, 251, .8),
              ]
              ) 
              ),
            child: Padding(
              padding: const EdgeInsets.only(left:6),
              child: Row(
                children: <Widget>[
                  CircleAvatar(
                    radius: 20,
                    backgroundColor:Colors.white,
                    backgroundImage:AssetImage('assets/images/facebook_circle-512.webp')
                  ),
                  SizedBox(width: 40.0),
                  Text("SIGNUP WITH FACEBOOK", style: TextStyle(color: Colors.white, fontSize: 20,),
                  ), 
                ]
              ),
              ), 
            ),

            //Email button
            SizedBox(height: 30,),
            Container(
              height: 50,
              decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(10),
              gradient: LinearGradient(
                colors: [
                  Color.fromRGBO(102, 224, 255, 1),
                  Color.fromRGBO(102, 224, 255, 1)
                  // Color.fromRGBO(143, 148, 251, 1),
                  // Color.fromRGBO(143, 148, 251, .6),
                ]
              ) 
              ),
              child: Padding(
                padding: const EdgeInsets.only(left:95),
                child: Row(
                  children: <Widget>[
                    // Icon(
                    //   Icons.email,
                    //     color: Colors.white,
                    // ),
                    SizedBox(width: 60.0),
                    FlatButton(
                      onPressed: ()=>{
                        Navigator.pushNamed(context, '/register/1')
                      },
                      child: Text("SIGNUP ", style: TextStyle(color: Colors.white, fontSize: 20,)),
                    )
                    // Center(
                    //   child: Center(child:Text("SIGNUP ", style: TextStyle(color: Colors.white, fontSize: 20,)),
                    //   ),
                    // ), 
                  ]
                ),
              ),  
            ), 

            //Divider
            Row(children: <Widget>[
              Expanded(
                child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, right: 15.0),
                  child: Divider(
                    color: Colors.black,
                    height: 80,
                  )
                ),
              ),
                          
              Text("OR"),
                
              Expanded(
                child: new Container(
                  margin: const EdgeInsets.only(left: 15.0, right: 10.0),
                  child: Divider(
                  color: Colors.black,
                  height: 50,
                  )
                ),
              ),
            ]
            ),
            
            //LOGIN BUTTON
            SizedBox(height: 10,),
            Container(
              height: 50,
              decoration: BoxDecoration(
                borderRadius: BorderRadius.circular(10),
                gradient: LinearGradient(
                  colors: [
                    Color.fromRGBO(102, 224, 255, 1),
                    Color.fromRGBO(102, 224, 255, 1)
                    // Color.fromRGBO(143, 138, 255, 1),
                    // Color.fromRGBO(143, 138, 255, 1),
                  ]
                ) 
              ),
              child: Padding(
                padding: const EdgeInsets.all(1.0),
                child:Column(
                  children: <Widget>[
                    FlatButton(onPressed: () {
                      print("hi");
                      Navigator.pushNamed(context, '/login');
                    },                                
                      child: Text("SIGNIN", style: TextStyle(color: Colors.white, fontSize: 20, fontWeight: FontWeight.bold),
                      ),
                    ),  
                  ]
                ),
              ),  
            ),  
          ]
        ),
      ),
    );

  }
}
