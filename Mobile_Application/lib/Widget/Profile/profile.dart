import 'package:aqua/Services/userservice.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:flutter/material.dart';




class Profile extends StatefulWidget {
  Profile({Key key}) : super(key: key);

  @override
  _ProfileState createState() => _ProfileState();
}

class _ProfileState extends State<Profile> {
  UserService service;
  Map userData;
  bool loader=true;

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    service=UserService();
    getUser();
  }
  Future<void> getUser()async{
    // String email="phpelendagama@gmail.com"; 
    dynamic temp =await service.getUser();
    setState(() {
      userData=temp;
    });
    setState(() {
      loader=false;
    });
    print(userData);

  }

  @override
  Widget build(BuildContext context) {
    var size = MediaQuery.of(context).size;
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        title: Text('Profile'),
        backgroundColor: Colors.blue,
        elevation: 0.0,
        actions: <Widget>[
          PopupMenuButton<String>(
            onSelected: (value)async => {
              await Navigator.pushNamed(context, "/editProfile"),
              getUser()

            },
            itemBuilder: (BuildContext context) {
              return [PopupMenuItem<String>(
                value: "Edit Profile",
                child: Text("Edit Profile"),
              )];
            },
          )
          // IconButton(
          //   icon: Icon(Icons.more_vert),
          //   onPressed: () { 
              
          //   },
          // )
        ],
      ),
      body:loader?
      Center(
        child: SpinKitWanderingCubes(
                    color: Colors.lightBlue,
                    size: 50.0,)
      )
      :Stack(
        children: <Widget>[
          Container(
            height: size.height*.35,
            decoration: BoxDecoration(
              color: Colors.blue
            ),
          ),
          
          Row(
            mainAxisAlignment: MainAxisAlignment.start,
            crossAxisAlignment: CrossAxisAlignment.start,
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 20.0, top: 20),
                child: CircleAvatar(                            
                  radius: 50,
                  backgroundImage: userData['gender']=='M'?AssetImage('assets/images/male.jpg'):AssetImage('assets/profile.jpg'),
                ),
              ),
              Padding(
                padding: const EdgeInsets.only(top:40.0,left: 38.0),
                child: Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: <Widget>[
                    Text(
                      userData['name'].toString(),
                      style: TextStyle(
                        fontWeight: FontWeight.bold,
                        fontSize: 23,
                        color: Colors.white
                      ),
                    ),
                    Text(
                     userData['email'].toString(),
                      style: TextStyle(
                        fontSize: 15,
                        color: Colors.white,
                      ),
                    ),
                  ],
                ),
              ),
              
            ],
          ),
          SizedBox(height:10),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 80.0, top: 150.0, right: 50.0, bottom: 270.0),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Text(
                         userData['age'].toString()+' yrs',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(height:10),
                        Text(
                         'AGE',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ), 
                      ],
                    ),
                    Container(
                      color: Colors.white,
                      width: 0.2,
                      height: 22,
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          userData['weight'].toString() +' kg',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(height:10),
                        Text(
                          'WEIGHT',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),   
                      ],
                    ),
                    Container(
                      color: Colors.white,
                      width: 0.2,
                      height: 22,
                    ),
                    Column(
                      children: <Widget>[
                        Text(
                          userData['height'].toString()+ ' cm',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 20,
                          ),
                        ),
                        SizedBox(height:10),
                        Text(
                         'HEIGHT',
                          style: TextStyle(
                            color: Colors.white,
                            fontWeight: FontWeight.bold,
                            fontSize: 10,
                          ),
                        ),   
                      ],
                    )
                  ],
                ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: EdgeInsets.only(top:270.0,left:30.0,bottom: 40.0),
                  child: Text(
                    'My Schedule',
                    style: TextStyle(
                      fontWeight:FontWeight.w500,
                      fontSize: 20.0
                    ),
                  ),
              ),
            ],
          ),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 50.0, top: 350.0, right: 50.0,bottom: 90.0 ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            Icon(
                              Icons.access_alarm,
                              color: Colors.blue,
                            ),
                            Container(
                              color: Colors.white,
                              width: 10.0,
                              height: 22,
                            ),
                            Text(
                              'WakeUp Time',
                              style: TextStyle(
                                fontSize:20.0,
                                // letterSpacing:2.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                              )
                            )
                          ],
                        ),
                        SizedBox(height:10),
                        Text(
                          userData['wakupTime'].toString(),
                          style: TextStyle(
                            //color: Colors.white,         
                            fontSize: 15,
                          ),
                        ), 
                      ],
                    ),
                  Container(
                    color: Colors.black,
                    width: 1.0,
                    height: 22,
                  ),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          Icon(
                            Icons.access_time,
                            color: Colors.blue,
                          ),
                          Container(
                            color: Colors.white,
                            width: 10.0,
                            height: 22,
                          ),
                          Text(
                            'Bed Time',
                            style: TextStyle(
                              fontSize:20.0,
                              fontWeight: FontWeight.bold,
                              //letterSpacing:2.0,
                              color: Colors.blue
                            )
                          )
                        ],
                      ),
                      SizedBox(height:10),
                        Text(
                          userData['sleepTime'].toString(),
                          style: TextStyle(
                          //color: Colors.white,           
                          fontSize: 15.0,
                          ),
                        ),   
                     ],
                    )
                  ],
                ),
                
              ),
            ],
          ),
          SizedBox(height: 10.0,),
          Column(
            children: <Widget>[
              Padding(
                padding: const EdgeInsets.only(left: 40.0, top: 450.0, right: 40.0,bottom: 90.0 ),
                child: Row(
                  mainAxisAlignment: MainAxisAlignment.spaceBetween,
                  children: <Widget>[
                    Column(
                      children: <Widget>[
                        Row(
                          children: <Widget>[
                            // Icon(
                            //   Icons.access_alarm,
                            //   color: Colors.indigo,
                            // ),
                            // Container(
                            //   color: Colors.white,
                            //   width: 10.0,
                            //   height: 22,
                            // ),
                            Text(
                              'Breakfast',
                              style: TextStyle(
                                fontSize:20.0,
                                // letterSpacing:2.0,
                                fontWeight: FontWeight.bold,
                                color: Colors.blue,
                              )
                            )
                          ],
                        ),
                        SizedBox(height:10),
                        Text(
                          userData['breakfastTime'],
                          style: TextStyle(
                            //color: Colors.white,         
                            fontSize: 15,
                          ),
                        ), 
                      ],
                    ),
                  Container(
                    color: Colors.black,
                    width: 1.0,
                    height: 22,
                  ),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // Icon(
                          //   Icons.access_time,
                          //   color: Colors.indigo,
                          // ),
                          // Container(
                          //   color: Colors.white,
                          //   width: 10.0,
                          //   height: 22,
                          // ),
                          Text(
                            'Lunch',
                            style: TextStyle(
                              fontSize:20.0,
                              fontWeight: FontWeight.bold,
                              //letterSpacing:2.0,
                              color: Colors.blue
                            )
                          )
                        ],
                      ),
                      SizedBox(height:10),
                        Text(
                          userData['lunchTime'],
                          style: TextStyle(
                          //color: Colors.white,           
                          fontSize: 15.0,
                          ),
                        ),   
                     ],
                    ),
                    Container(
                    color: Colors.black,
                    width: 1.0,
                    height: 22,
                  ),
                  Column(
                    children: <Widget>[
                      Row(
                        children: <Widget>[
                          // Icon(
                          //   Icons.access_time,
                          //   color: Colors.indigo,
                          // ),
                          // Container(
                          //   color: Colors.white,
                          //   width: 10.0,
                          //   height: 22,
                          // ),
                          Text(
                            'Dinner',
                            style: TextStyle(
                              fontSize:20.0,
                              fontWeight: FontWeight.bold,
                              //letterSpacing:2.0,
                              color: Colors.blue,
                            )
                          )
                        ],
                      ),
                      SizedBox(height:10),
                        Text(
                          userData['dinnerTime'],
                          style: TextStyle(
                          //color: Colors.white,           
                          fontSize: 15.0,
                          ),
                        ),   
                     ],
                    ),
                    
                  ],
                ),
                
              ),
            ],
          ),
        ],
      ),
      
      floatingActionButton: FloatingActionButton(
        onPressed: ()async {
          await Navigator.pushNamed(context, '/dailyschedule');
          getUser();
        },
        backgroundColor: Colors.blue,
        child: Icon(Icons.edit)
      ),  
      
    );
  }     
}