import 'package:aqua/Services/userservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class EditProfile extends StatefulWidget {
  EditProfile({Key key}) : super(key: key);
  @override
  _EditProfileState createState() => _EditProfileState();
}
class _EditProfileState extends State<EditProfile> {
  UserService service;
  Map userData;
  bool loader=true;
  bool update=false;
  final name = TextEditingController();
  final weight = TextEditingController();
  final height =TextEditingController();
  final _formKey = GlobalKey<FormState>();
  
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    service=UserService();
    getUser();
  }
  Future<void> getUser()async{
     
    dynamic temp =await service.getUser();
    setState(() {
      userData=temp;
    });
    print(userData);
    setState(() {
      loader=false;
    });

  }
  Future<int> updateProfile()async{
    setState(() {
      update=true;
    });
    Map<String,String> data=new Map<String,String>();
    data['name']=name.text==""?userData['name']:name.text;
    data['weight']=weight.text==""?userData['weight']:weight.text;
    data['height']=height.text==""?userData['height']:height.text;
    print(data);
    if(name.text==""&&height.text==""&&weight.text==""){
      return 0;
    }

    dynamic result=await service.updateProfile(data);
    setState(() {
      update=false;
    });
    if(result['status']==true){
      return 1;
    }else{
      return 2;
    }
  }
  @override
  Widget build(BuildContext context) {
   
    return Scaffold(
      backgroundColor: Colors.blue,
      appBar: AppBar(
        backgroundColor: Colors.blue,
        elevation: 0.0,
      ),
      body:loader?
      Center(
        child:SpinKitWanderingCubes(
                    color: Colors.white,
                    size: 50.0,)
      )
      :ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left:40.0),
            child:Row(
              children: <Widget>[
                Text('Edit',
                  style: TextStyle(
                    color: Colors.white,
                    fontWeight: FontWeight.bold,
                    fontSize: 25.0
                  )
                ),
                SizedBox(width:10.0),
                Text('Profile',
                  style: TextStyle(
                    color: Colors.white,
                    fontSize: 25.0
                  )
                ),
              ],
            ),
          ),
          SizedBox(height:40.0),
          Container(
           height: MediaQuery.of(context).size.height - 150.0,
           decoration: BoxDecoration(
             color: Colors.white,
             borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
           ),
           child: ListView(
              primary: false,
              padding: EdgeInsets.only(left:25.0, right:20.0),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top:55.0),
                  child: Container(
                    height:MediaQuery.of(context).size.height - 300.0,
                    child:Form(
                      key: _formKey,
                      child:Column(
                      children: <Widget>[
                        Text(
                          'NAME',
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          )
                        ),
                        Center(
                          child: TextFormField(
                            controller: name,
                            decoration: new InputDecoration(
                              
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue[200],width:5.0),
                              ),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[300],width: 2.0)),
                              //labelText: 'Email Address',
                              hintText: userData['name'].toString(),
                            ),
                          ),
                        ),
                        SizedBox(height: 40.0,),
                        Text(
                          'WEIGHT',
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Center(
                          child: TextFormField(
                            controller: weight,
                            decoration: new InputDecoration(
                              suffixText: 'Kg',
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue[200],width:5.0),
                              ),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[300],width: 2.0)),
                              //labelText: 'Email Address',
                              hintText: userData['weight'].toString()+' kg',
                            ),
                          ),
                        ),
                        SizedBox(height: 40.0,),
                        Text(
                          'HEIGHT',
                          style: TextStyle(
                            color: Colors.blue,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Center(
                          child: TextFormField(
                            controller: height,
                            decoration: new InputDecoration(
                              suffixText:'cm',
                              focusedBorder: OutlineInputBorder(
                                borderSide: BorderSide(color: Colors.blue[200],width:5.0),
                              ),
                              enabledBorder: OutlineInputBorder(borderSide: BorderSide(color: Colors.grey[300],width: 2.0)),
                              //labelText: 'Email Address',
                              hintText: userData['height'].toString()+' cm',
                            ),
                          ),
                        ),
                        
                      ],
                    )
                  )
                  ),
                ),
               Builder(builder: (context) =>
                
                  FlatButton(
                          child:update?SpinKitThreeBounce(color: Colors.white,size: 45.0,) 
                          :Text("Update",style: TextStyle(fontSize: 16),),
                          // color: Color.fromRGBO(102, 224, 255, 1),
                          color: Color(0xFF4B9DFE),
                          textColor: Colors.white,
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 15, bottom: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () async{
                            dynamic result=await updateProfile();
                            if(result==1){
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Successfully Updated"),
                                    trailing:Icon(Icons.beenhere)
                                  ) ,
                                backgroundColor: Colors.green,
                                )
                              );
                              Future.delayed(const Duration(seconds:2), ()=>{
                                Navigator.pop(context)
                              });
                              print("hello");
                              
                            }else if(result==0){
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Nothing to updat",style: TextStyle(color: Colors.white),),
                                    trailing:Icon(Icons.device_unknown,size: 50)
                                  ) ,
                                backgroundColor: Colors.green[400],
                                )
                              );
                            }else{
                              Scaffold.of(context).showSnackBar(
                                SnackBar(
                                  content:ListTile(
                                    title: Text("Something Went Wrong"),
                                    trailing:Icon(Icons.assignment_late)
                                  ) ,
                                backgroundColor: Colors.red,
                                )
                              );
                              print("hello");
                            }
                        
                          
                            // Find the Scaffold in the widget tree and use
                            // it to show a SnackBar.
                          
                          },
                        ),
                ), 
                
              ],
           )
          ),
          
          
        ]
      )
    );
  }
}   