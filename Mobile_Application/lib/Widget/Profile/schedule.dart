import 'package:flutter/material.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:intl/intl.dart';
import 'dart:async';

class Schedule extends StatefulWidget {
  Schedule({Key key}) : super(key: key);
  @override
  _ScheduleState createState() => _ScheduleState();
}

class _ScheduleState extends State<Schedule> {

  TimeOfDay _timeWakeup;
  TimeOfDay _timeBreakfast;
  TimeOfDay _timeLunch;
  TimeOfDay _timeDinner;
  TimeOfDay _timeSleep;
  Map formData;
  UserService service;
  Map shedule;
  bool loader=true;
  bool update=false;

  @override
  void initState(){
    super.initState();
    service=UserService();
    getShedule(); 
  }
  Future<void> getShedule()async{
    // String email="phpelendagama@gmail.com"; 
    dynamic temp =await service.getUser();
    setState((){
      shedule=temp;
    });
    var tem=(shedule['wakupTime'].toString()).split(" ");
    var wakup=(tem[0]).toString().split(':');
    wakup[0]=tem[1]=='AM'?wakup[0]:(int.parse(wakup[0])+12).toString();

    tem=(shedule['sleepTime'].toString()).split(" ");
    var sleep=(tem[0]).toString().split(':');
    print(sleep[0]);
    // sleep[0]=tem[1]=='AM'?sleep[0]:(int.parse(sleep[0])+12).toString();

    tem=(shedule['breakfastTime'].toString()).split(" ");
    var breakfast=(tem[0]).toString().split(':');
    breakfast[0]=tem[1]=='AM'?breakfast[0]:(int.parse(breakfast[0])+12).toString();

    tem=(shedule['lunchTime'].toString()).split(" ");
    var lunch=(tem[0]).toString().split(':');
    lunch[0]=tem[1]=='AM'?lunch[0]:(int.parse(lunch[0])+12).toString();

    tem=(shedule['dinnerTime'].toString()).split(" ");
    var dinner=(tem[0]).toString().split(':');
    dinner[0]=tem[1]=='AM'?dinner[0]:(int.parse(dinner[0])+12).toString();


    setState(() {
      _timeWakeup=new TimeOfDay(hour: int.parse(wakup[0]), minute: int.parse(wakup[1]));
    });

    setState(() {
      _timeSleep=new TimeOfDay(hour: int.parse(sleep[0]), minute: int.parse(sleep[1]));
    });

    setState(() {
      _timeBreakfast=new TimeOfDay(hour: int.parse(breakfast[0]), minute: int.parse(breakfast[1]));
    });

    setState(() {
      _timeLunch=new TimeOfDay(hour: int.parse(lunch[0]), minute: int.parse(lunch[1]));
    });
    setState(() {
      _timeDinner=new TimeOfDay(hour: int.parse(dinner[0]), minute: int.parse(dinner[1]));
    });

    setState(() {
      loader=false;
    });

    print(shedule);

  }
  bool compare(TimeOfDay a,TimeOfDay b){
    if(a.hour>b.hour){
      return false;
    }else if(a.hour==b.hour){
      if(a.minute>b.minute){
        return false;
      }if(a.minute==b.minute){
        return false;
      }else{
        return true;
      }  
    }else{
      return true;
    }
  }
  Future<bool> updateShedule(BuildContext context)async{
    setState(() {
      update=true;
    });
    if(compare(_timeWakeup,_timeBreakfast)){
      if(compare(_timeBreakfast,_timeLunch)){
        if(compare(_timeLunch,_timeDinner)){
          if(compare(_timeDinner,_timeSleep)){
              
          }else{
            Scaffold.of(context).showSnackBar(
              SnackBar(
                content:ListTile(
                  title: Text("Please select valid schedule"),
                  trailing:Icon(Icons.assignment_late)
                ) ,
              backgroundColor: Colors.red,
              )
            );
            setState(() {
              update=false;
            });
            return false;
          }
        }else{
          Scaffold.of(context).showSnackBar(
                SnackBar(
                  content:ListTile(
                    title: Text("Please select valid schedule"),
                    trailing:Icon(Icons.assignment_late)
                  ) ,
                backgroundColor: Colors.red,
                )
              );
              setState(() {
                update=false;
              });
              return false;
        }
      }else{
        Scaffold.of(context).showSnackBar(
                SnackBar(
                  content:ListTile(
                    title: Text("Please select valid schedule"),
                    trailing:Icon(Icons.assignment_late)
                  ) ,
                backgroundColor: Colors.red,
                )
              );
              setState(() {
                update=false;
              });
              return false;
      }
    }else{
      Scaffold.of(context).showSnackBar(
                SnackBar(
                  content:ListTile(
                    title: Text("Please select valid schedule"),
                    trailing:Icon(Icons.assignment_late)
                  ) ,
                backgroundColor: Colors.red,
                )
              );
              setState(() {
                update=false;
              });
              return false;
    }
    Map<String,String> data=new Map<String,String>();
    data['wakupTime']=formatTimeOfDay(_timeWakeup);
    data['sleepTime']=formatTimeOfDay(_timeSleep);
    data['breakfastTime']=formatTimeOfDay(_timeBreakfast);
    data['lunchTime']=formatTimeOfDay(_timeLunch);
    data['dinnerTime']=formatTimeOfDay(_timeDinner);
  
    dynamic result=await service.updateShedule(data);
    setState(() {
      update=false;
    });
    if(result['status']==true){
      Scaffold.of(context).showSnackBar(
        SnackBar(
          
          content:ListTile(
            title: Text("Updated"),
            trailing:Icon(Icons.beenhere)
          ) ,
        backgroundColor: Colors.green,
        )
      );      
    }else{
      Scaffold.of(context).showSnackBar(
        SnackBar(
          content:ListTile(
            title: Text("Something Went Wrong"),
            trailing:Icon(Icons.assignment_late)
          ) ,
        backgroundColor: Colors.red,
        )
      );
      print("hello");
    }
    if(result['status']==true){
      return true;
    }else{

      return false;
    }
  }
  String formatTimeOfDay(TimeOfDay tod) {
    final now = new DateTime.now();
    final dt = DateTime(now.year, now.month, now.day, tod.hour, tod.minute);
    final format = DateFormat.jm();  //"6:00 AM"
    return format.format(dt);
}

  @override
  
  Widget build(BuildContext context) {
    
    return Scaffold(  
      backgroundColor: Colors.blue,
      appBar: AppBar(
        //title: Text('Reset Schedule'),
        backgroundColor: Colors.blue,
        elevation: 0.0,          
      ),
      body: loader?
      Center(
        
        child: SpinKitWanderingCubes(
                    color: Colors.white,
                    size: 50.0,)
      )
      :ListView(
        children: <Widget>[
          Padding(
            padding: EdgeInsets.only(left:40.0),
            child: Row(
              children: <Widget>[
                Text('Reset',
                style: TextStyle(
                  color: Colors.white,
                  fontWeight: FontWeight.bold,
                  fontSize: 25.0
                )),
                SizedBox(width:10.0),
                 Text('Schedule',
                style: TextStyle(
                  color: Colors.white,
                  fontSize: 25.0
                )),
              ]
            ),
          ), 
          SizedBox(height:40.0),
          Container(
            height:MediaQuery.of(context).size.height - 150.0,
            decoration: BoxDecoration(
              color: Colors.white,
              borderRadius: BorderRadius.only(topLeft: Radius.circular(75.0)),
            ),
            child: ListView(
              primary: false,
              padding: EdgeInsets.only(left: 25.0, right: 20.0),
              children: <Widget>[
                Padding(
                  padding: EdgeInsets.only(top:45.0) ,
                  child: Container(
                    height:MediaQuery.of(context).size.height - 300.0,
                    child: ListView(
                      children:[
                        Center(
                          child:ListTile(
                            leading: CircleAvatar(                            
                              radius: 25,
                              backgroundImage: AssetImage('assets/wakeup.png'),
                            ),
                            title: Text(
                              'Wakeup Time ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                              ),
                            ),
                            subtitle:  Text('${formatTimeOfDay(_timeWakeup)}'),
                            dense: true,
                            trailing: Icon(Icons.timer),
                            onTap: _pickTimeWakup,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child:ListTile(
                            leading: CircleAvatar(                            
                              radius: 25,
                              backgroundImage: AssetImage('assets/breakfast.png'),
                            ),
                            title: Text(
                              'Breakfast',
                              style: TextStyle(
                                fontSize: 20.0,
                                fontWeight: FontWeight.bold,
                              ),
                            ),
                            subtitle:  Text('${formatTimeOfDay(_timeBreakfast)}'),
                            trailing: Icon(Icons.timer),
                            onTap: _pickTimeBreakfast,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child:ListTile(
                            leading: CircleAvatar(                            
                              radius: 25,
                              backgroundImage: AssetImage('assets/lunch.png'),
                            ),
                            title: Text(
                              'Lunch ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                              ),
                            ),
                            subtitle:  Text('${formatTimeOfDay(_timeLunch)}'),
                            dense: true,
                            trailing: Icon(Icons.timer),
                            onTap: _pickTimeLunch,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child:ListTile(
                            leading: CircleAvatar(                            
                              radius: 25,
                              backgroundImage: AssetImage('assets/dinner.png'),
                            ),
                            title: Text(
                              'Dinner ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                              ),
                            ),
                            subtitle:  Text('${formatTimeOfDay(_timeDinner)}'),
                            dense: true,
                            trailing: Icon(Icons.timer),
                            onTap: _pickTimeDinner,
                          ),
                        ),
                        SizedBox(height: 20,),
                        Center(
                          child:ListTile(
                            leading: CircleAvatar(                            
                              radius: 25,
                              backgroundImage: AssetImage('assets/bed.png'),
                            ),
                            title: Text(
                              'Bed Time ',
                              style: TextStyle(
                                fontWeight: FontWeight.bold,
                                fontSize: 20.0
                              ),
                            ),
                            subtitle:  Text('${formatTimeOfDay(_timeSleep)}'),
                            dense: true,
                            trailing: Icon(Icons.timer),
                            onTap: _pickTimeSleep,
                          ),
                        ),
                        
                      ],
                    )
                  ),
                ),
                SizedBox(height: 50,),
                Builder(builder: (context) =>
                
                  FlatButton(
                          child:update?SpinKitThreeBounce(color: Colors.white,size: 45.0,) 
                          :Text("Update",style: TextStyle(fontSize: 16),),
                          // color: Color.fromRGBO(102, 224, 255, 1),
                          color: Color(0xFF4B9DFE),
                          textColor: Colors.white,
                          padding: EdgeInsets.only(
                              left: 38, right: 38, top: 15, bottom: 15),
                          shape: RoundedRectangleBorder(
                              borderRadius: BorderRadius.circular(5)),
                          onPressed: () async{
                            dynamic result=await updateShedule(context);
                            
                            // Find the Scaffold in the widget tree and use
                            // it to show a SnackBar.
                          
                          },
                        ),
                ),
                
              ],
            )
          )
        ]
      ),
    );
  }  
  _pickTimeWakup() async {
    TimeOfDay time = await showTimePicker(
      context: context, 
      initialTime:_timeWakeup,
      builder: (BuildContext context, Widget child){
      return Theme(data: ThemeData(
        
      ),child:child);
    }); 
    if(time != null)
    setState(() {
      _timeWakeup =time;
    });
  }
  _pickTimeBreakfast() async {
    TimeOfDay time = await showTimePicker(
      context: context, 
      initialTime:_timeBreakfast,
      builder: (BuildContext context, Widget child){
      return Theme(data: ThemeData(
        
      ),child:child);
    }); 
    if(time != null)
    setState(() {
      _timeBreakfast =time;
    });
  }
  _pickTimeLunch() async {
    TimeOfDay time = await showTimePicker(
      context: context, 
      initialTime:_timeLunch,
      builder: (BuildContext context, Widget child){
      return Theme(data: ThemeData(
        
      ),child:child);
    }); 
    if(time != null)
    setState(() {
      _timeLunch =time;
    });
  }
  _pickTimeDinner() async {
    TimeOfDay time = await showTimePicker(
      context: context, 
      initialTime:_timeDinner,
      builder: (BuildContext context, Widget child){
      return Theme(data: ThemeData(
        
      ),child:child);
    }); 
    if(time != null)
    setState(() {
      _timeDinner =time;
    });
  }
  _pickTimeSleep() async {
    TimeOfDay time = await showTimePicker(
      context: context, 
      initialTime:_timeSleep,
      builder: (BuildContext context, Widget child){
      return Theme(data: ThemeData(
        
      ),child:child);
    }); 
    if(time != null)
    setState(() {
      _timeSleep =time;
    });
  }   
  
}