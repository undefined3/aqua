import 'dart:async';
import 'dart:ffi';
import 'package:aqua/Widget/History/Services/week.dart';
import 'package:aqua/Widget/History/Services/month.dart';
import 'package:aqua/Widget/History/Services/historyService.dart';
import 'package:aqua/Widget/History/weekly.dart';
import 'package:aqua/Widget/History/monthly.dart';
import 'package:aqua/Widget/History/pieChart.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class History extends StatefulWidget {
  @override
  _HistoryState createState() => _HistoryState();
}

class _HistoryState extends State<History> {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Usage History'),
        backgroundColor: Colors.blue,
        centerTitle: true,
      ),
      body: BodyWidget(),
      floatingActionButton: FloatingActionButton(
        onPressed: () {
          setState(() {
            selectedWidgetMarker = WidgetMarker.pie;
          });
        },
        backgroundColor: Colors.blue[700],
        child: Icon(Icons.pie_chart),
      ),
    );
  }
}

enum WidgetMarker { weekly, monthly, yearly, pie }
WidgetMarker selectedWidgetMarker;

class BodyWidget extends StatefulWidget {
  @override
  _BodyWidgetState createState() => _BodyWidgetState();
}

class _BodyWidgetState extends State<BodyWidget> {
  bool load = true;
  HistoryService service;
  List<WeekUsage> week = [];
  int weekTotal = 0;
  double weekAverage;
  int monthTotal = 0;
  double monthAverage;
  List<MonthUsage> month = [];
  Map<String, dynamic> color = new Map<String, dynamic>();

  Future<void> setWeek() async {
    List<WeekUsage> temp = await service.setWeek();
    print("get week executed");
    for (int i = 0; i < temp.length; i++) {
      temp[i].barColor = this.color[temp[i].day.toString()];
      weekTotal += temp[i].usage;
    }
    weekAverage = double.parse((weekTotal / 7).toStringAsFixed(2));
    temp.forEach((element) {
      print(element.barColor);
    });
    setState(() {
      this.week = temp;
    });
    selectedWidgetMarker = WidgetMarker.weekly;
    setState(() {
      this.load = false;
    });
  }

  Future<void> setMonth() async {
    List<MonthUsage> temp = await service.setMonth();

    for (int i = 0; i < temp.length; i++) {
      temp[i].barColor = this.color[temp[i].month.toString()];
      monthTotal += temp[i].usage;
    }
    monthAverage = double.parse((monthTotal/ 12).toStringAsFixed(2));

    setState(() {
      this.month = temp;
    });
  }

  Future<void> setPie() async{
    for (int i = 0; i < data.length; i++) {
      data[i].color = this.color[data[i].source.toString()];
    }
  }

  @override
  void initState() {
    super.initState();
    service = HistoryService();
    // selectedWidgetMarker = WidgetMarker.weekly;

    color['Mon'] = charts.ColorUtil.fromDartColor(Colors.teal[800]);
    color['Tue'] = charts.ColorUtil.fromDartColor(Colors.teal[900]);
    color['Wed'] = charts.ColorUtil.fromDartColor(Colors.teal[400]);
    color['Thu'] = charts.ColorUtil.fromDartColor(Colors.teal[600]);
    color['Fri'] = charts.ColorUtil.fromDartColor(Colors.teal[300]);
    color['Sat'] = charts.ColorUtil.fromDartColor(Colors.teal[500]);
    color['Sun'] = charts.ColorUtil.fromDartColor(Colors.teal[200]);

    color['Jan'] = charts.ColorUtil.fromDartColor(Colors.cyan[200]);
    color['Feb'] = charts.ColorUtil.fromDartColor(Colors.cyan[400]);
    color['Mar'] = charts.ColorUtil.fromDartColor(Colors.cyan[300]);
    color['Apr'] = charts.ColorUtil.fromDartColor(Colors.cyan[700]);
    color['May'] = charts.ColorUtil.fromDartColor(Colors.cyan[600]);
    color['Jun'] = charts.ColorUtil.fromDartColor(Colors.cyan[500]);
    color['Jul'] = charts.ColorUtil.fromDartColor(Colors.cyan[200]);
    color['Aug'] = charts.ColorUtil.fromDartColor(Colors.cyan[100]);
    color['Sep'] = charts.ColorUtil.fromDartColor(Colors.cyan[300]);
    color['Oct'] = charts.ColorUtil.fromDartColor(Colors.cyan[100]);
    color['Nov'] = charts.ColorUtil.fromDartColor(Colors.cyan[700]);
    color['Dec'] = charts.ColorUtil.fromDartColor(Colors.cyan[400]);

    color['Water'] = charts.ColorUtil.fromDartColor(Colors.blue[100]);
    color['Fruit juice'] = charts.ColorUtil.fromDartColor(Colors.orangeAccent);
    color['Cofee'] = charts.ColorUtil.fromDartColor(Colors.brown);
    color['Tea'] = charts.ColorUtil.fromDartColor(Colors.amber);

    setWeek();
    setMonth();
    setPie();
    service.setDailyUse();
  }

  Widget build(BuildContext context) {
    return Column(
      children: <Widget>[
        Row(
          mainAxisAlignment: MainAxisAlignment.center,
          children: <Widget>[
            FlatButton(
              onPressed: () {
                setState(() {
                  selectedWidgetMarker = WidgetMarker.weekly;
                });
              },
              child: Text(
                'Weekly',
                style: TextStyle(color: Colors.blue[900]),
              ),
              color: Colors.blue[300],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
            ),
            FlatButton(
              onPressed: () {
                setState(() {
                  selectedWidgetMarker = WidgetMarker.monthly;
                });
              },
              child: Text(
                'Monthly',
                style: TextStyle(color: Colors.blue[900]),
              ),
              color: Colors.blue[300],
              shape: RoundedRectangleBorder(
                  borderRadius: BorderRadius.circular(20.0)),
            ),
          ],
        ),
        load
            ? Container(
                child: Center(
                    child: SpinKitWave(color: Colors.lightBlue, size: 30.0)))
            : Container(
                child: getCustomContainer(),
              ),
        SizedBox(height: 20.0),
      ],
    );
  }

  Widget getWeeklyWidget() {
    return Container(
      height: 200,
      color: Colors.red,
    );
  }

  Widget getMonthlyWidget() {
    return Container(
      height: 300,
      color: Colors.green,
    );
  }

  Widget getYearWidget() {
    return Container(
      height: 400,
      color: Colors.blue,
    );
  }

  Widget getPieChart() {
    return Container(
      height: 200,
      color: Colors.red,
    );
  }

  Widget getCustomContainer() {
    switch (selectedWidgetMarker) {
      case WidgetMarker.weekly:
        // List<WeekUsage> data =[
        //   WeekUsage(day: 'Mon', usage: 900.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[800])),
        //   WeekUsage(day: 'Tue', usage: 600.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[900])),
        //   WeekUsage(day: 'Wed', usage: 1200.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[600])),
        //   WeekUsage(day: 'Thu', usage: 2800.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[200])),
        //   WeekUsage(day: 'Fri', usage: 1100.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[500])),
        //   WeekUsage(day: 'Sat', usage: 1400.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[400])),
        //   WeekUsage(day: 'Sun', usage: 2000.00, barColor: charts.ColorUtil.fromDartColor(Colors.blue[300]))
        // ];
        // print(week);
        return Weekly(data: week, average: weekAverage);

      case WidgetMarker.monthly:
        // List<MonthUsage> data = [
        //   MonthUsage(month: 'Jan', usage: 65.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[400])),
        //   MonthUsage(month: 'Feb', usage: 50.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[800])),
        //   MonthUsage(month: 'Mar', usage: 70.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[300])),
        //   MonthUsage(month: 'Apr', usage: 65.5, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[600])),
        //   MonthUsage(month: 'May', usage: 55.65, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[300])),
        //   MonthUsage(month: 'Jun', usage: 45.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[500])),
        //   MonthUsage(month: 'Jul', usage: 65.4, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[700])),
        //   MonthUsage(month: 'Aug', usage: 53.2, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[800])),
        //   MonthUsage(month: 'Sep', usage: 72.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[200])),
        //   MonthUsage(month: 'Oct', usage: 69.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[400])),
        //   MonthUsage(month: 'Nov', usage: 54.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[600])),
        //   MonthUsage(month: 'Dec', usage: 56.0, barColor: charts.ColorUtil.fromDartColor(Colors.cyan[800])),
        // ];

        return Monthly(data: month, average: monthAverage,);

      case WidgetMarker.yearly:
        return getYearWidget();

      case WidgetMarker.pie:
        return PieOutsideChart(data: data);
    }

    return getWeeklyWidget();
  }
}

class dailyWaterUsage {
  String source;
  double percentage;
  charts.Color color;

  dailyWaterUsage(this.source, this.percentage);
}

final data = [
  new dailyWaterUsage('Water', 80.0),
  new dailyWaterUsage('Fruit juice', 15.0),
  new dailyWaterUsage('Cofee', 1.25),
  new dailyWaterUsage('Tea', 3.75)
];
