import 'Services/week.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;


class Weekly extends StatefulWidget {

  final List<WeekUsage> data; 
  final double average;
  Weekly({ this.data, this.average});
  
  @override
  _WeeklyState createState() => _WeeklyState(data: data, average: average);
}

class _WeeklyState extends State<Weekly> {
  
  List<WeekUsage> data; 
  final double average;
  _WeeklyState({ this.data, this.average });
  

  @override
  Widget build(BuildContext context) {

    List<charts.Series<WeekUsage, String>> series = [ 
      charts.Series(
        data: data, 
        id: 'weekUsage',
        domainFn: (WeekUsage usage, _) =>
        usage.day,
        measureFn: (WeekUsage usage, _) =>
        usage.usage,
        colorFn: (WeekUsage usage, _) =>
        usage.barColor
        )
    ];

    return 
    Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(
        children: <Widget>[
          Container(
            height: 400,
            child: Expanded(
              child: charts.BarChart( series, animate: true )
              ),
          ),
          SizedBox(height:28.0),
          Text('Daily average of this week : $average liters',
          style: TextStyle(
            // fontStyle: FontStyle.italic,
            color: Colors.deepPurple,
            fontWeight: FontWeight.w800,
            fontSize: 16.0
          ),)
        ],
      ),
      
    );
  }
}