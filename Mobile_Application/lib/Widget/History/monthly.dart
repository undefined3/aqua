import 'package:aqua/Widget/History/Services/month.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class Monthly extends StatefulWidget {
  final List<MonthUsage> data;
  final double average;
  Monthly({this.data, this.average});

  @override
  _MonthlyState createState() => _MonthlyState(data: data, average:average);
}

class _MonthlyState extends State<Monthly> {
  List<MonthUsage> data;
  final double average;
  _MonthlyState({this.data, this.average});

  @override
  Widget build(BuildContext context) {
    List<charts.Series<MonthUsage, String>> series = [
      charts.Series(
          data: data,
          id: 'monthlyUsage',
          domainFn: (MonthUsage usage, _) => usage.month,
          measureFn: (MonthUsage usage, _) => usage.usage,
          colorFn: (MonthUsage usage, _) => usage.barColor)
    ];

    return Padding(
      padding: const EdgeInsets.all(10.0),
      child: Column(children: <Widget>[
        Container(
          height: 400,
          child: Expanded(
              child: charts.BarChart(
            series,
            animate: true,
          )),
        ),
        SizedBox(height: 28.0,),
        Text('Monthly average of this year : $average liters',
          style: TextStyle(
            // fontStyle: FontStyle.italic,
            color: Colors.deepPurple,
            fontWeight: FontWeight.w800,
            fontSize: 16.0
          ),)
      ]),
    );
  }
}
