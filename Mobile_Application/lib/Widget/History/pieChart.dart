import 'package:aqua/Widget/History/history.dart';
// import 'package:aqua/Widget/signup/height_styles.dart';

// import 'Services/week.dart';
import 'package:flutter/material.dart';
import 'package:charts_flutter/flutter.dart' as charts;

class PieOutsideChart extends StatefulWidget {
  final List<dailyWaterUsage> data;
  PieOutsideChart({this.data});

  @override
  _PieOutsideChartState createState() => _PieOutsideChartState(data: data);
}

class _PieOutsideChartState extends State<PieOutsideChart> {
  List<dailyWaterUsage> data;
  _PieOutsideChartState({this.data});

  @override
  Widget build(BuildContext context) {
    List<charts.Series<dailyWaterUsage, String>> series = [
      charts.Series(
        data: data,
        id: 'dailyWater',
        domainFn: (dailyWaterUsage usage, _) => usage.source,
        measureFn: (dailyWaterUsage usage, _) => usage.percentage,
        colorFn: (dailyWaterUsage usage, _) => usage.color,
        labelAccessorFn: (dailyWaterUsage row, _) =>
            '${row.source}: ${row.percentage}',
      )
    ];

    return Padding(
      padding: const EdgeInsets.all(15.0),
      child: Container(
          height: 400,
          // width: 700,
          child: Expanded(
              child: charts.PieChart(
            series,
            animate: true,
            behaviors: [
              new charts.DatumLegend(),
            ],
          ))),
    );
  }
}
