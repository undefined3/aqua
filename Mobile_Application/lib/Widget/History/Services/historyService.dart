import 'dart:io';

import 'package:aqua/Widget/History/Services/month.dart';
import 'package:aqua/Widget/History/Services/week.dart';
import 'package:http/http.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'dart:convert';
import '../../../Services/comonvar.dart';

class HistoryService{
  CommoneVar comnvar=CommoneVar();
  List<WeekUsage> week=[];

 Future<SharedPreferences> _prefs = SharedPreferences.getInstance();

  Future<List> setWeek() async {
    final SharedPreferences prefs = await _prefs;

    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    // get data from backend server
    String url = "${comnvar.getUrl()}history/week";
    Response result = await get(url,headers: requestHeaders);
    
    print(json.decode(result.body));
    // create a list from received data
    List<dynamic> temp = json.decode(result.body);
      
    // convert list to WeekUsage type
    List data = temp.map((e) => WeekUsage(day:e['day'].toString(),usage:int.parse(e['usage']))).toList();
    
    return data;

  }

  Future<List> setMonth() async {
    final SharedPreferences prefs = await _prefs;

    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    // get data from backend server
    String url = "${comnvar.getUrl()}history/month";
    Response result  = await get(url,headers: requestHeaders);
    print(result.body);


    // create a list from recieved data
    List<dynamic> temp = json.decode(result.body);


    // convert list to MontUsage type
    List data = temp.map((e) => MonthUsage(month:e['month'].toString(),usage:int.parse(e['usage']))).toList();

    return data;

  }

  Future<List> setDailyUse() async {


    final SharedPreferences prefs = await _prefs;

    String token=prefs.getString('jwt');
    Map<String, String> requestHeaders = {
      'Content-type': 'application/json',
      'Accept': 'application/json',
      'Authorization':'Barear $token'
    };
    // get data from backend server
    String url = "${comnvar.getUrl()}history/dailyUse";
    Response response = await get(url,headers: requestHeaders);
    print(response.body);

  }

}