import 'package:charts_flutter/flutter.dart' as charts;

class MonthUsage {
  String month;
  int usage;
  charts.Color barColor;

  MonthUsage ({ this.month, this.usage }); 

}