import 'package:charts_flutter/flutter.dart' as charts;

class WeekUsage {
  String day;
  int usage;
  charts.Color barColor;

  WeekUsage({ this.day, this.usage });  

}