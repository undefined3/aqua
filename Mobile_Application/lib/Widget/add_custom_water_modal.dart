import 'package:aqua/Services/dashboardService.dart';
import 'package:flutter/material.dart';

class AddWaterModal extends StatefulWidget {
  String picture;
  int volume;
  AddWaterModal({this.picture, @required this.volume}) {
    this.picture = picture;
  }

  @override
  _AddWaterModalState createState() => _AddWaterModalState();
}

class _AddWaterModalState extends State<AddWaterModal> {
  DashboardService dashboardService;

  var summary;
  Future updateWater(volume, status) async {
    var temp = await dashboardService.addWater(volume, status);
    print(temp);
  }

  int count = 0;
  addWater(volume) {
    setState(() {
      count++;
    });
 
    updateWater(volume, true);
  }

  reduceWater(volume) {
    if (count >= 1) {
      setState(() {
        count--;
      });

      updateWater(volume, false);
    }
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    dashboardService = DashboardService();
  }

  @override
  Widget build(BuildContext context) {
    return Container(
      child: SizedBox(
        height: 300,
        child: Container(
          color: Colors.white,
          child: Column(
            children: [
              Row(
                mainAxisAlignment: MainAxisAlignment.center,
                children: [
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: IconButton(
                        icon: Icon(
                          Icons.add_circle,
                          size: 40,
                        ),
                        onPressed: () => addWater(widget.volume)),
                  ),
                  Image.network(
                    widget.picture,
                    width: 200,
                  ),
                  Padding(
                    padding: const EdgeInsets.all(8.0),
                    child: IconButton(
                        icon: Icon(
                          Icons.remove_circle,
                          size: 40,
                        ),
                        onPressed: () => reduceWater(widget.volume)),
                  ),
                ],
              ),
              Center(
                child: Text(
                  count.toString(),
                  style: TextStyle(fontSize: 30),
                ),
              )
            ],
          ),
        ),
      ),
    );
  }
}
