import 'dart:async';

import 'package:aqua/Services/NotificationPlugin.dart';
import 'package:aqua/Services/dashboardService.dart';
import 'package:aqua/Services/leaderboardService.dart';
import 'package:aqua/Widget/add_custom_water_modal.dart';
import 'package:aqua/widget/drawer.dart';
import 'package:flutter/material.dart';
import 'package:percent_indicator/percent_indicator.dart';
import 'package:liquid_progress_indicator/liquid_progress_indicator.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:modal_bottom_sheet/modal_bottom_sheet.dart';
import '../Services/userservice.dart';
import 'package:intl/intl.dart';

class Dashboard extends StatefulWidget {
  Dashboard({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _DashboardState createState() => _DashboardState();
}

class _DashboardState extends State<Dashboard> {
  int _counter = 0;
  DashboardService dashboardService;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

  UserService userService;
  String name = "";
  
  var dailyTarget = "0";
  var dailyUsage = "0";
  var waterLevel="0";
  var shedule=[];

  var summary;
  Future getData() async {
    Map temp = await dashboardService.getSummary();

    setState(() {
      summary = temp;
      dailyTarget=temp['dailyTarget']..toString();
      dailyUsage=temp["dailyUsage"]..toString();
      waterLevel=temp["waterLevel"].toString();
      shedule = summary['data']['shedule'];

    });

    // print(summary['waterData'][0]["dailyTarget"]);
    // dailyTarget=summary['waterData'][0] ["dailyTarget"];
    // dailyUsage=summary['waterData'][0]["dailyUsage"];
  }
 


  @override
  void initState() {
    super.initState();
    notificationPlugin
        .setListenerForLowerVersions(onNotificationInLowerVersions);
    notificationPlugin.setOnNotificationClick(onNotificationClick);
    userService = UserService();
    dashboardService = DashboardService();
    getName();
    getData();
    sendNotificaton();
    sec5Timer();
  }

  getName() async {
    dynamic data = await userService.decodeData();
    print(data);
    setState(() {
      name = data['name'].split(" ")[0];
    });
  }

  sec5Timer() {
    Timer.periodic(Duration(seconds: 5), (timer) async {
      await getData();
      // await notificationPlugin.showNotification();
    });
  }

  Text subheading(String title) {
    return Text(
      title,
      style: TextStyle(
          color: LightColors.kDarkBlue,
          fontSize: 20.0,
          fontWeight: FontWeight.w700,
          letterSpacing: 1.2),
    );
  }

  static CircleAvatar calendarIcon() {
    return CircleAvatar(
      radius: 25.0,
      backgroundColor: LightColors.kGreen,
      child: Icon(
        Icons.invert_colors,
        size: 20.0,
        color: Colors.white,
      ),
    );
  }

  getTarget() {
    var value = double.parse(dailyUsage) / double.parse(dailyTarget) * 100;
    if (value > 1) {
      return 1.0;
    }
    return value;
  }

  @override
  Widget build(BuildContext context) {
    // summary = ModalRoute.of(context).settings.arguments;
    double width = MediaQuery.of(context).size.width;

    return Scaffold(
      // backgroundColor: Colors.lightBlue[300],
      backgroundColor: Colors.white,
      key: _scaffoldKey,

      drawer: MyDrawer(),
      body: SafeArea(
        child: Column(
          children: <Widget>[
            TopContainer(
              height: 200,
              width: width,
              child: Column(
                  mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                  children: <Widget>[
                    Row(
                      mainAxisAlignment: MainAxisAlignment.spaceBetween,
                      children: <Widget>[
                        IconButton(
                            icon: Icon(Icons.menu,
                                color: Colors.black, size: 40.0),
                            onPressed: () =>
                                _scaffoldKey.currentState.openDrawer())
                        // Icon(Icons.menu, color: Colors.black, size: 40.0),
                        // Icon(Icons.search, color: Colors.white, size: 25.0),
                      ],
                    ),
                    Padding(
                      padding: const EdgeInsets.symmetric(
                          horizontal: 0, vertical: 0.0),
                      child: Row(
                        crossAxisAlignment: CrossAxisAlignment.center,
                        mainAxisAlignment: MainAxisAlignment.spaceEvenly,
                        children: <Widget>[
                          CircularPercentIndicator(
                            radius: 90.0,
                            lineWidth: 5.0,
                            animation: true,
                            percent: 0.75,
                            circularStrokeCap: CircularStrokeCap.round,
                            progressColor: LightColors.kRed,
                            backgroundColor: LightColors.kDarkYellow,
                            center: CircleAvatar(
                                backgroundColor: LightColors.kBlue,
                                radius: 35.0,
                                backgroundImage: NetworkImage(
                                    "https://encrypted-tbn0.gstatic.com/images?q=tbn%3AANd9GcSt0P9VVsmft2Q0gP7fMmZWG6nO0P2Xet61NS2CA6ihg0eAiGbu&usqp=CAU")),
                          ),
                          Column(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            children: <Widget>[
                              Container(
                                child: Text(
                                  name,
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 30.0,
                                    color: LightColors.kDarkBlue,
                                    fontWeight: FontWeight.w800,
                                  ),
                                ),
                              ),
                              Container(
                                child: Text(
                                  'Good Morning!',
                                  textAlign: TextAlign.start,
                                  style: TextStyle(
                                    fontSize: 16.0,
                                    color: Colors.black45,
                                    fontWeight: FontWeight.w400,
                                  ),
                                ),
                              ),
                            ],
                          )
                        ],
                      ),
                    )
                  ]),
            ),
            Expanded(
              child: SingleChildScrollView(
                child: Column(
                  children: <Widget>[
                    Container(
                      color: Colors.transparent,
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: Column(
                        children: <Widget>[
                          Row(
                            crossAxisAlignment: CrossAxisAlignment.center,
                            mainAxisAlignment: MainAxisAlignment.spaceBetween,
                            children: <Widget>[
                              subheading('Daily Drinkings'),
                              GestureDetector(
                                onTap: () {
                                  // Navigator.push(
                                  //   context,
                                  //   MaterialPageRoute(
                                  //       builder: (context) => CalendarPage()),
                                  // );
                                },
                                child: calendarIcon(),
                              ),
                            ],
                          ),
                          SizedBox(height: 15.0),
                          _AnimatedLiquidLinearProgressIndicator(
                            dailyUsage: waterLevel.split('.')[0],
                            dailyTarget: "600",
                          ),
                          SizedBox(
                            height: 15.0,
                          ),
                          Card(
                            elevation: 20,
                            shape: RoundedRectangleBorder(
                                borderRadius: BorderRadius.circular(20)),
                            child: Padding(
                              padding: const EdgeInsets.all(8.0),
                              child: Column(
                                children: [
                                  TaskColumn(
                                    icon: Icons.alarm,
                                    iconBackgroundColor: LightColors.kRed,
                                    title: 'To Drink',
                                    subtitle: dailyTarget + " ml",
                                  ),
                                  SizedBox(
                                    height: 15.0,
                                  ),
                                  TaskColumn(
                                    icon: Icons.blur_circular,
                                    iconBackgroundColor:
                                        LightColors.kDarkYellow,
                                    title: 'Drank',
                                    subtitle: dailyUsage + " ml",
                                  ),
                                  SizedBox(height: 15.0),
                                ],
                              ),
                            ),
                          ),
                          SizedBox(
                            height: 35,
                          ),
                          Padding(
                            padding: const EdgeInsets.all(10.0),
                            child: Container(
                              // color: Colors.red,
                              child: SizedBox(
                                height: 120,
                                child: ListView(
                                  scrollDirection: Axis.horizontal,
                                  children: [
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8.0, top: 8.0),
                                      child: SizedBox(
                                        width: 100,
                                        child: GestureDetector(
                                          onTap: () => {
                                            // showMaterialModalBottomSheet(
                                            //   context: context,
                                            //   builder: (context,
                                            //           scrollController) =>
                                            //       AddWaterModal(
                                            //           picture:
                                            //               "https://i.pinimg.com/474x/d3/95/c4/d395c45f3a6078ac0cea8014f29aa970.jpg"),
                                            // )
                                          },
                                          child: GestureDetector(
                                            onTap: () => {
                                              showMaterialModalBottomSheet(
                                                context: context,
                                                builder: (context,
                                                        scrollController) =>
                                                    AddWaterModal(
                                                        picture:
                                                            "https://i.pinimg.com/474x/d3/95/c4/d395c45f3a6078ac0cea8014f29aa970.jpg",
                                                        volume: 200),
                                              )
                                            },
                                            child: Card(
                                              elevation: 5,
                                              shadowColor: Colors.blue,
                                              shape: RoundedRectangleBorder(
                                                  borderRadius:
                                                      BorderRadius.circular(
                                                          50)),
                                              child: Padding(
                                                padding:
                                                    const EdgeInsets.all(20.0),
                                                child: Image.network(
                                                    'https://i.pinimg.com/474x/d3/95/c4/d395c45f3a6078ac0cea8014f29aa970.jpg'),
                                              ),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8.0, top: 8.0),
                                      child: SizedBox(
                                        width: 100,
                                        child: GestureDetector(
                                          onTap: () => {
                                            showMaterialModalBottomSheet(
                                              context: context,
                                              builder:
                                                  (context, scrollController) =>
                                                      AddWaterModal(
                                                picture:
                                                    "https://i.ibb.co/kqbZ84P/cup-304886-1280.png",
                                                volume: 100,
                                              ),
                                            )
                                          },
                                          child: Card(
                                            elevation: 5,
                                            shadowColor: Colors.blue,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: Padding(
                                                padding:
                                                    const EdgeInsets.all(20.0),
                                                child: Image.network(
                                                    'https://i.ibb.co/kqbZ84P/cup-304886-1280.png')),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8.0, top: 8.0),
                                      child: SizedBox(
                                        width: 100,
                                        child: GestureDetector(
                                          onTap: () => {
                                            showMaterialModalBottomSheet(
                                              context: context,
                                              builder:
                                                  (context, scrollController) =>
                                                      AddWaterModal(
                                                picture:
                                                    "https://images.assetsdelivery.com/compings_v2/berkut2011/berkut20111802/berkut2011180200136.jpg",
                                                volume: 50,
                                              ),
                                            )
                                          },
                                          child: Card(
                                            elevation: 5,
                                            shadowColor: Colors.blue,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Image.network(
                                                  'https://images.assetsdelivery.com/compings_v2/berkut2011/berkut20111802/berkut2011180200136.jpg'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                    Padding(
                                      padding: const EdgeInsets.only(
                                          bottom: 8.0, top: 8.0),
                                      child: SizedBox(
                                        width: 100,
                                        child: GestureDetector(
                                          onTap: () => {
                                            showMaterialModalBottomSheet(
                                              context: context,
                                              builder:
                                                  (context, scrollController) =>
                                                      AddWaterModal(
                                                picture:
                                                    "https://i.ibb.co/0Zp6Vp1/tumbler-cup-png-transparent-images-7206-pngio-glass-with-straw-png-1200-1200.jpg",
                                                volume: 10,
                                              ),
                                            )
                                          },
                                          child: Card(
                                            elevation: 5,
                                            shadowColor: Colors.blue,
                                            shape: RoundedRectangleBorder(
                                                borderRadius:
                                                    BorderRadius.circular(50)),
                                            child: Padding(
                                              padding:
                                                  const EdgeInsets.all(20.0),
                                              child: Image.network(
                                                  'https://i.ibb.co/0Zp6Vp1/tumbler-cup-png-transparent-images-7206-pngio-glass-with-straw-png-1200-1200.jpg'),
                                            ),
                                          ),
                                        ),
                                      ),
                                    ),
                                  ],
                                ),
                              ),
                            ),
                          ),
                        ],
                      ),
                    ),
                    Container(
                      color: Colors.transparent,
                      padding: EdgeInsets.symmetric(
                          horizontal: 20.0, vertical: 10.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: <Widget>[
                          // subheading('Active Projects'),
                          SizedBox(height: 5.0),
                          Row(
                            children: <Widget>[
                              ActiveProjectsCard(
                                cardColor: LightColors.kGreen,
                                loadingPercent: double.parse(waterLevel) / 600.0,
                                title: 'Bottle Water Level',
                                subtitle:
                                    (waterLevel).toString() +
                                        'ml to finish',
                              ),
                              SizedBox(width: 20.0),
                              ActiveProjectsCard(
                                cardColor: LightColors.kRed,
                                // loadingPercent: 0.5,
                                loadingPercent: getTarget(),
                                title: 'Daily Target',
                                subtitle: 'Hurry!',
                              ),
                            ],
                          ),
                          Row(
                            children: <Widget>[
                              ActiveProjectsCard(
                                cardColor: LightColors.kDarkYellow,
                                loadingPercent: 0.80,
                                title: 'Water Quality ',
                                subtitle: 'Good',
                              ),
                              SizedBox(width: 20.0),
                              ActiveProjectsCard(
                                cardColor: LightColors.kBlue,
                                loadingPercent: 0.9,
                                title: 'Bottle battery level',
                                subtitle: 'Good health',
                              ),
                            ],
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ),
            ),
          ],
        ),
      ),
    );
  }

  onNotificationInLowerVersions(ReceivedNotification receivedNotification) {
    print('Notification Received ${receivedNotification.id}');
  }

  onNotificationClick(String payload) {
    print('Payload $payload');
    // Navigator.push(context, MaterialPageRoute(builder: (coontext) {
    //   return NotificationScreen(
    //     payload: payload,
    //   );
    // }));
  }

  sendNotificaton() async {
    DateTime now = DateTime.now();
    String hour = DateFormat('kk').format(now);
    String minute = DateFormat('mm').format(now);
    String year = DateFormat('yyyy').format(now);

    DateTime currentTime =
        new DateFormat('HH:mm').parse(DateTime.now().toString());

    DateTime time1 = new DateFormat("HH:mm").parse("06:00");
    var time2 = new DateFormat("HH:mm").parse("06:30");
    var time3 = new DateFormat("HH:mm").parse("08:00");
    var time5 = new DateFormat("HH:mm").parse("12:30");
    var time4 = new DateFormat("HH:mm").parse("14:00");
    var time6 = new DateFormat("HH:mm").parse("19:00");
    var time7 = new DateFormat("HH:mm").parse("20:30");
    var time8 = new DateFormat("HH:mm").parse("23:40");

    if (currentTime.isAfter(time1) && currentTime.isBefore(time2)) {
      print("TRUE");
      if (shedule[0]['target'] > shedule[0]['archived']) {
        print("1");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time2) && currentTime.isBefore(time3)) {
      if (shedule[1]['target'] > shedule[1]['archived']) {
        print("2");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time3) && currentTime.isBefore(time4)) {
      if (shedule[2]['target'] > shedule[2]['archived']) {
        print("3");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time4) && currentTime.isBefore(time5)) {
      if (shedule[3]['target'] > shedule[3]['archived']) {
        print("4");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time5) && currentTime.isBefore(time6)) {
      if (shedule[4]['target'] > shedule[4]['archived']) {
        print("5");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time6) && currentTime.isBefore(time7)) {
      if (shedule[5]['target'] > shedule[5]['archived']) {
        print("6");
        await notificationPlugin.showNotification();
      }
    } else if (currentTime.isAfter(time7) && currentTime.isBefore(time8)) {
      if (shedule[6]['target'] > shedule[6]['archived']) {
        print("7");
        await notificationPlugin.showNotification();
      }
    } else {
      print("8");
      await notificationPlugin.showNotification();
    }
  }
}

class TopContainer extends StatelessWidget {
  final double height;
  final double width;
  final Widget child;
  final EdgeInsets padding;
  TopContainer({this.height, this.width, this.child, this.padding});

  @override
  Widget build(BuildContext context) {
    return Container(
      padding:
          padding != null ? padding : EdgeInsets.symmetric(horizontal: 20.0),
      decoration: BoxDecoration(
          // color: Colors.blue,
          // color: Colors.lightBlue[100],
          gradient: LinearGradient(
            colors: [Colors.blue, Colors.white],
            begin: Alignment.bottomRight,
            end: Alignment.bottomLeft,
          ),
          borderRadius: BorderRadius.only(
            bottomRight: Radius.circular(40.0),
            bottomLeft: Radius.circular(40.0),
          )),
      height: height,
      width: width,
      child: child,
    );
  }
}

class TaskColumn extends StatelessWidget {
  final IconData icon;
  final Color iconBackgroundColor;
  final String title;
  final String subtitle;
  TaskColumn({
    this.icon,
    this.iconBackgroundColor,
    this.title,
    this.subtitle,
  });
  @override
  Widget build(BuildContext context) {
    return Row(
      crossAxisAlignment: CrossAxisAlignment.center,
      children: <Widget>[
        CircleAvatar(
          radius: 20.0,
          backgroundColor: iconBackgroundColor,
          child: Icon(
            icon,
            size: 15.0,
            color: Colors.white,
          ),
        ),
        SizedBox(width: 10.0),
        Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Text(
              title,
              style: TextStyle(
                fontSize: 16.0,
                fontWeight: FontWeight.w700,
              ),
            ),
            Text(
              subtitle,
              style: TextStyle(
                  fontSize: 14.0,
                  fontWeight: FontWeight.w500,
                  color: Colors.black45),
            ),
          ],
        )
      ],
    );
  }
}

class ActiveProjectsCard extends StatelessWidget {
  final Color cardColor;
  final double loadingPercent;
  final String title;
  final String subtitle;

  ActiveProjectsCard({
    this.cardColor,
    this.loadingPercent,
    this.title,
    this.subtitle,
  });

  @override
  Widget build(BuildContext context) {
    return Expanded(
      flex: 1,
      child: Container(
        margin: EdgeInsets.symmetric(vertical: 10.0),
        padding: EdgeInsets.all(15.0),
        height: 200,
        decoration: BoxDecoration(
          color: cardColor,
          borderRadius: BorderRadius.circular(40.0),
        ),
        child: Column(
          mainAxisAlignment: MainAxisAlignment.spaceBetween,
          crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            Padding(
              padding: const EdgeInsets.all(10.0),
              child: CircularPercentIndicator(
                animation: true,
                radius: 75.0,
                percent: loadingPercent,
                lineWidth: 5.0,
                circularStrokeCap: CircularStrokeCap.round,
                backgroundColor: Colors.white10,
                progressColor: Colors.white,
                center: Text(
                  '${(loadingPercent * 100).round()}%',
                  style: TextStyle(
                      fontWeight: FontWeight.w700, color: Colors.white),
                ),
              ),
            ),
            Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: <Widget>[
                Text(
                  title,
                  style: TextStyle(
                    fontSize: 14.0,
                    color: Colors.white,
                    fontWeight: FontWeight.w700,
                  ),
                ),
                Text(
                  subtitle,
                  style: TextStyle(
                    fontSize: 12.0,
                    color: Colors.white54,
                    fontWeight: FontWeight.w400,
                  ),
                ),
              ],
            ),
          ],
        ),
      ),
    );
  }
}

class LightColors {
  static const Color kLightYellow = Color(0xFFFFF9EC);
  static const Color kLightYellow2 = Color(0xFFFFE4C7);
  static const Color kDarkYellow = Color(0xFFF9BE7C);
  static const Color kPalePink = Color(0xFFFED4D6);

  static const Color kRed = Color(0xFFE46472);
  static const Color kLavender = Color(0xFFD5E4FE);
  static const Color kBlue = Color(0xFF6488E4);
  static const Color kLightGreen = Color(0xFFD9E6DC);
  static const Color kGreen = Color(0xFF309397);

  static const Color kDarkBlue = Color(0xFF0D253F);
}

class _AnimatedLiquidLinearProgressIndicator extends StatefulWidget {
  String dailyTarget;
  String dailyUsage;

  _AnimatedLiquidLinearProgressIndicator({this.dailyUsage, this.dailyTarget});

  @override
  State<StatefulWidget> createState() =>
      _AnimatedLiquidLinearProgressIndicatorState();
}

class _AnimatedLiquidLinearProgressIndicatorState
    extends State<_AnimatedLiquidLinearProgressIndicator>
    with SingleTickerProviderStateMixin {
  AnimationController _animationController;

  @override
  void initState() {
    super.initState();

    _animationController = AnimationController(
      vsync: this,
      duration: Duration(seconds: 10),
    );

    _animationController.addListener(() => setState(() {}));
    _animationController.repeat();
  }

  @override
  void dispose() {
    _animationController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    final percentage =
        (int.parse(widget.dailyUsage) / int.parse(widget.dailyTarget)) * 100;

    // print(percentage*100);

    return Center(
      child: Container(
        width: double.infinity,
        height: 75.0,
        padding: EdgeInsets.symmetric(horizontal: 24.0),
        child: LiquidLinearProgressIndicator(
          value: percentage / 100,
          // value: _animationController.value,
          backgroundColor: Colors.white,
          valueColor: AlwaysStoppedAnimation(Colors.blue),
          borderRadius: 12.0,
          center: Text(
            percentage.toStringAsFixed(0) + " %",
            // "${percentage.toStringAsFixed(0)}%",
            style: TextStyle(
              color: Colors.lightBlueAccent,
              fontSize: 20.0,
              fontWeight: FontWeight.bold,
            ),
          ),
        ),
      ),
    );
  }
}
