import 'package:aqua/Services/userservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class Feedbackk extends StatefulWidget {
  Feedbackk({Key key}) : super(key: key);

  @override
  _FeedbackkState createState() => _FeedbackkState();
}

class _FeedbackkState extends State<Feedbackk> {

  UserService userService=new UserService();

  var val="";
  bool load=false;
  final _formKey = GlobalKey<FormState>();
  final feedback = TextEditingController();
  addFeedback()async{
    if(val==""){
      return;
    }  
    setState(() {
      load=true;  
    });
    print(feedback.text);
    await userService.sendFeedBack(val,feedback.text.toString());
    feedback.clear();

    setState(() {
      load=false;
      val="";
    });
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomPadding: false,
      appBar: AppBar(
        iconTheme: IconThemeData(color: Colors.black),
        elevation: 2.0,
        backgroundColor: Colors.lightBlue,
        centerTitle: true,
        title: Text('Feedback', style: TextStyle(fontWeight: FontWeight.bold)),
        leading: IconButton(
          onPressed: () {},
          icon: Icon(Icons.arrow_back),
        ),
      ),

      //first part of the body

      body: Padding(
        padding: EdgeInsets.all(16.0),
        //child: SingleChildScrollView(
        child: ListView(
          // crossAxisAlignment: CrossAxisAlignment.start,
          children: <Widget>[
            SizedBox(height: 10.0),
            Text(
              "Please select the type of the feedback",
              style: TextStyle(color: Color(0xffc5c5c5)),
            ),
            SizedBox(height: 5.0),
            RadioListTile(
              
              groupValue:val,
              value:"Login trouble",
              onChanged: (value) => {
                setState(() {
                  val=value;
                })
              },
                  
              title:Text("Login trouble") ,
            ),
            RadioListTile(
              
              groupValue:val,
              value:"Personal profile",
              
              onChanged: (value) => {
                setState(() {
                  val=value;
                })
              },
                  
              title:Text("Personal profile") ,
            ),
            RadioListTile(
              
              groupValue:val,
              value:"Leadprofile Issues",
              onChanged: (value) => {
                setState(() {
                  val=value;
                })
              },

              title:Text("Leadprofile Issues") ,
            ),
            RadioListTile(
              
              groupValue:val,
              value:"Suggestion",

              onChanged: (value) => {
                setState(() {
                  val=value;
                })
              },
                  
              title:Text("Suggestion") ,
            ),
            RadioListTile(
              
              groupValue:val,
              value:"Other",
              onChanged: (value) => {
                setState(() {
                  val=value;
                })
              },
                  
              title:Text("Other") ,
            ),
            SizedBox(height: 20.0),
            Form(
              key:_formKey ,
              child:TextFormField(
                maxLines: 10,
                controller: feedback,
                validator: (val){
                  if(val.isEmpty){
                    return "Please enter your question or suggestion";
                  }else{
                    return null;
                  }
                },
                decoration: InputDecoration(
                    hintText: "Please describe the issue briefly",
                    hintStyle: TextStyle(
                      fontSize: 13.0,
                      color: Color(0xffc5c5c5),
                    ),
                    border: OutlineInputBorder(
                      borderSide: BorderSide(color: Color(0xffe5e5e5)),
                    )),
                ),
            ),
            
            SizedBox(height: 20.0),
            // buildNumberField(),
            Spacer(),
            Row(
              children: <Widget>[
                Expanded(
                  child:
                  
                   FlatButton(
                    onPressed: ()async {
                      if(_formKey.currentState.validate()){
                        await addFeedback();
                      }
                    },
                    color: Colors.blue,
                    padding: EdgeInsets.all(16.0),
                    child:load?
                    SpinKitThreeBounce(color: Colors.white,size: 45.0,):
                     Text(
                      "SUBMIT",
                      style: TextStyle(
                        color: Colors.white,
                        fontWeight: FontWeight.bold,
                      ),
                    ),
                  ),
                )
              ],
            )
          ],
        ),
      ),
      //),
    );
  }

//phone number input cage

  buildNumberField() {
    return TextField(
      style: TextStyle(
        color: Colors.black,
      ),
      decoration: InputDecoration(
        contentPadding: EdgeInsets.all(0.0),
        prefixIcon: Row(
          mainAxisSize: MainAxisSize.min,
          children: <Widget>[
            Container(
              decoration: BoxDecoration(
                border: Border(
                  right: BorderSide(
                    width: 1.0,
                    color: Color(0xffc5c5c5),
                  ),
                ),
              ),
              child: Row(
                mainAxisSize: MainAxisSize.min,
                children: <Widget>[
                  SizedBox(width: 10.0),
                  // Text(
                  //   "+91",
                  //   style: TextStyle(
                  //     fontWeight: FontWeight.bold,
                  //     color: Color(0xffc5c5c5),
                  //   ),
                  // ),
                  // Icon(
                  //   Icons.arrow_drop_down,
                  //   color: Colors.cyan,
                  // ),
                  SizedBox(width: 10.0),
                ],
              ),
            ),
            SizedBox(width: 10.0),
          ],
        ),
        hintStyle: TextStyle(
          fontSize: 14.0,
          color: Color(0xffc5c5c5),
        ),
        hintText: "Phone number",
        border: OutlineInputBorder(),
      ),
    );
  }

//first cage

  buildFeedbackForm() {
    return Container(
      height: 200.0,
      child: Stack(
        children: <Widget>[
          TextField(
            maxLines: 10,
            decoration: InputDecoration(
                hintText: "Please describe the issue briefly",
                hintStyle: TextStyle(
                  fontSize: 13.0,
                  color: Color(0xffc5c5c5),
                ),
                border: OutlineInputBorder(
                  borderSide: BorderSide(color: Color(0xffe5e5e5)),
                )),
          ),
          Align(
            alignment: Alignment.bottomCenter,
            child: Container(
              decoration: BoxDecoration(
                  border: Border(
                      top: BorderSide(
                width: 1.0,
                color: Color(0xffa6a6a6),
              ))),
              padding: EdgeInsets.all(8.0),
              child: Row(
                children: <Widget>[
                  Container(
                    decoration: BoxDecoration(
                      color: Color(0xffe5e5e5),
                      borderRadius: BorderRadius.circular(5.0),
                    ),
                    child: Padding(
                      padding: EdgeInsets.all(1.0),
                      child: FlatButton(
                        onPressed: () {},
                        child: Icon(
                          Icons.add,
                          color: Color(0xffa5a5a5),
                        ),
                      ),
                    ),
                  ),
                  // SizedBox(width: 10.0),
                  // Text(
                  //   "Upload screenshot (optional)",
                  //   style: TextStyle(
                  //     color: Color(0xffc5c5c5),
                  //   ),
                  // )
                ],
              ),
            ),
          )
        ],
      ),
    );
  }

//type of feedback

  buildCheckItem(title) {
    return Padding(
      padding: EdgeInsets.only(bottom: 10.0),
      child: Row(
        children: <Widget>[
          Icon(Icons.check_circle, color: Colors.blue),
          SizedBox(width: 10.0),
          Text(
            title,
            style: TextStyle(
              fontWeight: FontWeight.bold,
              color: Colors.blue,
            ),
          ),
        ],
      ),
    );
  }
}
