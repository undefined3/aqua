import 'dart:async';

import 'package:aqua/Services/dashboardService.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';

class LoadingPage extends StatefulWidget {
  LoadingPage({Key key}) : super(key: key);

  @override
  _LoadingPageState createState() => _LoadingPageState();
}

class _LoadingPageState extends State<LoadingPage> {
  UserService userService = UserService();

  


  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    Timer(Duration(seconds: 2), () => isLogin());
    
  }

  isLogin() async {
    bool status = await userService.isLogedIn();
    print(status);
    if (status) {
      Navigator.pushReplacementNamed(context, "/dasboard");
    } else {
      Navigator.pushReplacementNamed(context, "/intro");
    }
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        body: SafeArea(
            child: Container(
      margin: EdgeInsets.only(top: 100, left: 50.0, right: 50.0),
      child: Column(
        children: <Widget>[
          Container(
            margin: EdgeInsets.only(top: 30),
            child: Image(
              image: AssetImage('assets/images/Logo/new2.png'),
            ),
          ),
          Container(
              margin: EdgeInsets.only(top: 30),
              child: Text(
                "leading to a healthy generation",
                style: TextStyle(
                    fontSize: 20, color: Color.fromRGBO(102, 224, 255, 1)),
              )),
          Container(
            margin: EdgeInsets.only(top: 100),
            child: SpinKitCircle(
              color: Color.fromRGBO(102, 224, 255, 1),
              size: 45.0,
            ),
          )
        ],
      ),
    )));
  }
}
