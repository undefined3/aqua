import 'package:aqua/Services/dashboardService.dart';
import 'package:aqua/Services/userservice.dart';
import 'package:aqua/Widget/Leaderboard/leaderboard.dart';
import 'package:flutter/material.dart';
import 'package:shared_preferences/shared_preferences.dart';
import 'package:qrscan/qrscan.dart' as scanner;
import 'dart:async';

class MyDrawer extends StatefulWidget {
  @override
  _MyDrawerState createState() => _MyDrawerState();
}

class _MyDrawerState extends State<MyDrawer> {
  Map userData;
  UserService userService;
  bool load = false;
  DashboardService dashboardService;

  Future addBarcode(value) async {
    var temp = await dashboardService.addBarcode(value);
    print(temp);
  }

  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    userService = UserService();
    getUser();
    dashboardService = DashboardService();
  }

  Future<void> getUser() async {
    dynamic result = await userService.decodeData();
    print(result);
    setState(() {
      load = true;
    });
    setState(() {
      userData = result;
    });
  }

  @override
  Widget build(BuildContext context) {
    return Drawer(
      child: ListView(
        children: <Widget>[
          UserAccountsDrawerHeader(
            decoration: BoxDecoration(
                image: DecorationImage(
                    fit: BoxFit.cover,
                    image: NetworkImage(
                        'https://i.pinimg.com/originals/3f/0c/7e/3f0c7e525f1b771c5d94a7627a583144.jpg'))),
            accountName: Text(
              load ? userData['name'].toString() : "",
              style: TextStyle(fontSize: 30),
            ),
            accountEmail: Text(load ? userData['email'].toString() : ""),
            currentAccountPicture: load
                ? CircleAvatar(
                    child: Card(
                        elevation: 20,
                        color: Colors.transparent,
                        child: CircleAvatar(
                            radius: 55,
                            backgroundImage:
                                userData['gender'].toString() == 'M'
                                    ? AssetImage('assets/images/male.jpg')
                                    : AssetImage('assets/profile.jpg'))
                        //  Image.network(
                        //     'https://covidinspection.com/wp-content/uploads/2020/03/868320_people_512x512.png',),
                        ),
                  )
                : Text(""),
          ),
          ListTile(
              title: Text("Leadboard"),
              trailing: Icon(Icons.timeline),
              onTap: () {
                Navigator.pushNamed(context, '/leaderboard');
              }),
          ListTile(
              title: Text("Schedule"),
              trailing: Icon(Icons.calendar_today),
              onTap: () {
                Navigator.pushNamed(context, '/waterschedule');
              }),
          ListTile(
            title: Text("History"),
            trailing: Icon(Icons.update),
            onTap: () {
              Navigator.pushNamed(context, '/history');
            },
          ),
          ListTile(
            title: Text("Connct Bottle"),
            trailing: Icon(Icons.search),
            onTap: () async {
              addBarcode(await scanner.scan());
            },
          ),
          ListTile(
            title: Text("Profile"),
            trailing: Icon(Icons.account_circle),
            onTap: () {
              Navigator.pushNamed(context, '/profile');
            },
          ),
          ListTile(
            title: Text("Ask Question"),
            trailing: Icon(Icons.device_unknown),
            onTap: () {
              Navigator.pushNamed(context, '/feedbackk');
            },
          ),
          ListTile(
            title: Text("Logout"),
            trailing: Icon(Icons.exit_to_app),
            onTap: () async {
              await userService.logout();
              // final SharedPreferences prefs = await _prefs;
              // await prefs.setBool('log', false);
              // await prefs.setString('email',"");
              Navigator.pushReplacementNamed(context, '/login');
            },
          ),
        ],
      ),
    );
  }
}
