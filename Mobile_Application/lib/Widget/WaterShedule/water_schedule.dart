import 'package:aqua/Services/shedulService.dart';
import 'package:aqua/Widget/WaterShedule/watercard.dart';
import 'package:email_validator/email_validator.dart';
import 'package:flutter/material.dart';
import 'dart:ui';
import 'package:intl/intl.dart';
import 'package:flutter_local_notifications/flutter_local_notifications.dart';

class WaterSchedule extends StatefulWidget {
  @override
  _WaterScheduleState createState() => _WaterScheduleState();
}

class _WaterScheduleState extends State<WaterSchedule> {
  SheduleService sheduleService;
  List<String> shedule=new List<String>(); 
  List<int> targetList=new List<int>(); 
  List<int> drankList=new List<int>(); 
  List<int> currentStatus=new List<int>(); 

  final notification= new FlutterLocalNotificationsPlugin();
  bool spinner=true;
  @override
  void initState() {
    // TODO: implement initState
    super.initState();
    sheduleService=new SheduleService();
    getSheduledate();
   final settingsAndroid = AndroidInitializationSettings('app_icon');
    final settingsIOS = IOSInitializationSettings(
        onDidReceiveLocalNotification: (id, title, body, payload) =>
            onSelectNotification(payload));

    notification.initialize(
        InitializationSettings(settingsAndroid, settingsIOS),
        onSelectNotification: onSelectNotification);
  }
  Future onSelectNotification(String payload) async => {
    print(payload)
  };
  showNotification(){
     final androidChannelSpecifics = AndroidNotificationDetails(
      'your channel id',
      'your channel name',
      'your channel description',
      importance: Importance.Max,
      priority: Priority.High,
      ongoing: false,
      autoCancel: false,
    );
    final iOSChannelSpecifics = IOSNotificationDetails();
    notification.show(0,"Helll","HHHHHHHHHH",NotificationDetails(androidChannelSpecifics, iOSChannelSpecifics));

  }
  getSheduledate()async{

    List<String> tempList=new List<String>();
    List<int> tempTarget=new List<int>();
    List<int> tempDrank=new List<int>();
    List<int> tempstatus=new List<int>();

    try{
      Map temp=await sheduleService.getShedule();
    List tempshedule=temp['shedule'];
    var now = new DateTime.now();
    print(now);
    for(int i =0;i<7;i++){
      //  print(tempshedule[i]);
      var startDate = new DateTime.utc(now.year,now.month,now.day,tempshedule[i]['startTime']['hour'],tempshedule[i]['startTime']['minute']);

      var endDate = new DateTime.utc(now.year,now.month,now.day,tempshedule[i]['endTime']['hour'],tempshedule[i]['endTime']['minute']);
      print("--------");
      // print(now.millisecondsSinceEpoch);
      print(startDate);
      // print(now.compareTo(endDate));
      print(endDate);
      print("--------");

      if(now.millisecondsSinceEpoch>startDate.millisecondsSinceEpoch){
        print('\n\n\n\n\n\nhell\n\n\n\n\n\n');
      }else{
        print('\n\n\n\n\n\nhuuuuuu\n\n\n\n\n\n');
      }
      

      String shedulestr="Start ${DateFormat(' kk:mm a').format(startDate)}\nEnd   ${DateFormat(' kk:mm a').format(endDate)}";
      tempTarget.add(tempshedule[i]['target']);
      tempDrank.add(tempshedule[i]['archived']);
      // print((tempshedule[i]['target']).runtimeType);
      print(i);

      tempList.add(shedulestr);
    }
    print("hi");
    print(tempstatus);
    setState(() {
      drankList=tempDrank;
      targetList=tempTarget;
      shedule=tempList;
      spinner=false;
    });
    }catch(error){
      print(error);
      Navigator.pop(context);
    }
    //  print(temp);
  }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      appBar: AppBar(
        elevation: 0,
        backgroundColor: Colors.lightBlue,
        title: Text('Aqua', style: TextStyle(fontWeight: FontWeight.bold)),
        centerTitle: true,
      ),
      body:Stack(
        children: <Widget>[
          Expanded(
                  child: new Container(
                    alignment: Alignment.center,
                      // margin: const EdgeInsets.only(left: 10.0, right: 20.0),
                      child: VerticalDivider(
                        thickness: 2,
                        color: Colors.black,
                        // width: 200,
                      )),
          ),
          Column(children: <Widget>[

            // first
            SizedBox(height: 17,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),

            // second
            SizedBox(height: 10,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),
            
            // third
            SizedBox(height: 105,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),

            // fourth
            SizedBox(height: 110,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),

            // fifth
            SizedBox(height: 23,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),

            // Sixth
            SizedBox(height: 65,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),

            // seventh
            SizedBox(height: 80,),
            Container(
              color: Colors.white,
              alignment:Alignment.center,
              child: Icon(Icons.adjust,color: Colors.blue,),
            ),
          ],),
          
          
          // First box
          Container(
            child: WaterCard(timeText:shedule.length==0?"":shedule[0],target:targetList.length==0?0:targetList[0],done:drankList.length==0?0:drankList[0],spinner: spinner),
            margin:EdgeInsets.only(left: 250.0, right: 0.0,),
            
            height: 195,
            width: 150,
            
          ),
          Expanded(
            child: new Container(
                margin: const EdgeInsets.only(left: 220,
                right: 160,
                top:30
                ),
                child: Divider(
                  color: Colors.blue,
                  thickness: 3,
                  height: 0,
                )),
          ),


          // Second box
          Container(
            child:Card(
            shape: RoundedRectangleBorder(
            side: new BorderSide(color: Colors.blue, width: 2.0),
            borderRadius: BorderRadius.circular(15.0)),
            borderOnForeground: true,
            child:Padding(
              padding:EdgeInsets.only(top:10),
              child:Column(
              children: <Widget>[
                Text("Breakfast Break\n",style: TextStyle(fontSize: 15)),
                Text("${shedule.length==0?"":shedule[1]}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),)
              ],) ,  
            )
            
              
          ),
            margin:EdgeInsets.only(left: 10.0, right: 0.0,top:20),
            height: 90,
            width: 150,
          ),
          Expanded(
            child: new Container(
                margin: const EdgeInsets.only(left: 158,
                right: 220,
                top:65
                ),
                child: Divider(
                  color: Colors.blue,
                  thickness: 3,
                  height: 0,
                )),
          ),


          // Third box

          Container(
            child:WaterCard(timeText:shedule.length==0?"":shedule[2],target:targetList.length==0?0:targetList[2],done:drankList.length==0?0:drankList[2],spinner: spinner),
            margin:EdgeInsets.only(left: 10.0, right: 0.0,top:112),
            
            height: 195,
            width: 150,
            // color: Colors.red,
          ),
          Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 158.0,
                  right: 220, 
                  top:193
                  ),
                  child: Divider(
                    color: Colors.blue,
                    thickness: 3,
                    height: 0,
                  )),
          ),

          // Fourth box

          Container(
            child:Card(
            shape: RoundedRectangleBorder(
            side: new BorderSide(color: Colors.blue, width: 2.0),
            borderRadius: BorderRadius.circular(15.0)),
            borderOnForeground: true,
            // elevation: 10,
            child:Padding(
              padding:EdgeInsets.only(top:10),
              child:Column(
              children: <Widget>[
                Text("Lunch Break\n",style: TextStyle(fontSize: 15)),
                Text("${shedule.length==0?"":shedule[3]}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),)
              ],) ,  
            )
            
              
          ),
            margin:EdgeInsets.only(left: 10.0, right: 0.0,top:310),
            height: 90,
            width: 150,
            // color: Colors.red,
          ),
          Expanded(
            child: new Container(
                margin: const EdgeInsets.only(left: 158.0,
                right: 220,
                top:330
                ),
                child: Divider(
                  color: Colors.blue,
                  thickness:3,
                  height: 0,
                )),
          ),

          // Fifth box

          Container(
            child: WaterCard(timeText:shedule.length==0?"":shedule[4],target:targetList.length==0?0:targetList[4],done:drankList.length==0?0:drankList[4],spinner: spinner),
            margin:EdgeInsets.only(left: 250.0, right: 0.0,top:203),
            
            height: 195,
            width: 150,
            

          ),
          Expanded(
            
              child: new Container(
                   margin: const EdgeInsets.only(left: 220.0,
                    top:374,
                    right: 160,
                  ),
                  child: Divider(
                    color: Colors.blue,
                    thickness: 3,
                    height: 0,
                  )),
          ),
          
          
          
          // Sixth box
        //  Container(
        //    child:WaterCard(timeText:shedule.length==0?"":shedule[5],target:targetList.length==0?0:targetList[5],done:drankList.length==0?0:drankList[5],spinner: spinner),
        //     margin:EdgeInsets.only(left: 10.0, right: 0.0,top:405),
            
        //     height: 195,
        //     width: 150,

        //   ),

        Container(
            child:Card(
            shape: RoundedRectangleBorder(
            side: new BorderSide(color: Colors.blue, width: 2.0),
            borderRadius: BorderRadius.circular(15.0)),
            borderOnForeground: true,
            child:Padding(
              padding:EdgeInsets.only(top:10),
              child:Column(
              children: <Widget>[
                Text("Dinner Break\n",style: TextStyle(fontSize: 15)),
                Text("${shedule.length==0?"":shedule[5]}",style: TextStyle(fontWeight: FontWeight.bold,fontSize: 12),)
              ],) ,  
            )
            
              
          ),
            margin:EdgeInsets.only(left: 10.0, right: 0.0,top:420),
            height: 90,
            width: 150,
          ),
          Expanded(
              child: new Container(
                margin: const EdgeInsets.only(
                  left: 220,
                  right: 160,
                  top:568
                ),
                  child: Divider(
                    color: Colors.blue,
                    thickness: 3,
                    height: 0,
                  )),
          ),
          
          // Seven box
          Container(
            child: WaterCard(timeText:shedule.length==0?"":shedule[6],target:targetList.length==0?0:targetList[6],done:drankList.length==0?0:drankList[6],spinner: spinner),
            margin:EdgeInsets.only(left: 250.0, right: 0.0,top:405),
            height: 195,
            width: 150,
            // color: Colors.red,
          ),
          Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(
                  left: 158.0,
                  right: 220, 
                  top:464
                  ),
                  child: Divider(
                    color: Colors.blue,
                    thickness: 3,
                    height:0,
                  )),
          ),
              
        ],
      ),
    );
  }
}


