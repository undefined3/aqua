import 'package:flutter/material.dart';
import 'package:flutter_spinkit/flutter_spinkit.dart';


class WaterCard extends StatefulWidget {
  WaterCard({Key key,this.timeText,this.target,this.done,this.spinner}) : super(key: key);
  final String timeText;
  final int target;
  final int done;
  bool spinner;
  @override
  _WaterCardState createState() => _WaterCardState();
}

class _WaterCardState extends State<WaterCard> {
  @override
  Widget build(BuildContext context) {
    return Container(
       child: Card(
          shape: RoundedRectangleBorder(
          side: new BorderSide(color: Colors.blue, width: 2.0),
          borderRadius: BorderRadius.circular(15.0)),
          borderOnForeground: true,
          // elevation: 10,
          
          child:widget.spinner?
          Center(child:SpinKitWanderingCubes(
              color: Colors.lightBlue,
              size: 50.0,)
            ):Column(
              children: <Widget>[
                SizedBox(height: 15,),
                Text(widget.timeText,style: TextStyle(fontSize: 12,fontWeight: FontWeight.bold),),
                
                ListTile(
                  title: Text("Target"),
                  subtitle: Text("${widget.target}ml"),
                  leading: Icon(Icons.assignment,color: Colors.blue,),
                ),
                ListTile(
                  title: Text("Drank"),
                  subtitle: Text("${widget.done}ml"),
                  leading: Icon(Icons.assignment_turned_in,color: Colors.green,),
                ),
                // Text("iiiiii"),
                // Text("iiiiii"),
              ],
            ),
              
            )
    );
  }
}