import 'package:aqua/Widget/login/login.dart';
import 'package:flutter/material.dart';
import 'package:introduction_screen/introduction_screen.dart';

class IntroPage extends StatefulWidget {
  @override
  _IntroPageState createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  final introKey = GlobalKey<IntroductionScreenState>();

  void _onIntroEnd(context) {
    Navigator.of(context).push(
      MaterialPageRoute(builder: (_) => Login()),
    );
  }

  Widget _buildImage(String assetName) {
    return Align(
      child: Image.network(assetName, width: 350.0),
      // child: Image.asset('assets/$assetName.jpg', width: 350.0),
      alignment: Alignment.bottomCenter,
    );
  }

  @override
  Widget build(BuildContext context) {
    const bodyStyle = TextStyle(fontSize: 19.0);
    const pageDecoration = const PageDecoration(
      titleTextStyle: TextStyle(fontSize: 28.0, fontWeight: FontWeight.w700),
      bodyTextStyle: bodyStyle,
      descriptionPadding: EdgeInsets.fromLTRB(16.0, 0.0, 16.0, 16.0),
      pageColor: Colors.white,
      imagePadding: EdgeInsets.zero,
    );

    return IntroductionScreen(
      key: introKey,
      pages: [
        PageViewModel(
          title: "Add flavor to your pitcher.",
          body:
              "You can add a little bit of excitement and flavor by steeping fresh fruit, veggie slices, and herbs in your carafe",
          image: _buildImage(
              'https://images.all-free-download.com/images/graphicthumb/cups_and_water_vector_286640.jpg'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Drink a glass after every bathroom break.",
          body:
              "Start a habit by linking drinking water with one of your most common daily activities—going to the bathroom.",
          image: _buildImage('https://i.ibb.co/9grvsLw/5.jpg'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Sip before every meal",
          body:
              "If you are making dinner at home, sip while you cook and prep. If you're out at a restaurant, ask for water when the server comes around to take drink orders.",
          image: _buildImage(
              'https://i.ibb.co/Pj0Jb2Z/2.png'),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "AQUA",
          body: "Your Smart Water Bottle",
          image: _buildImage(
              'https://i.ibb.co/PFYYDK5/1.png'),
          // footer: RaisedButton(
          //   onPressed: () {
          //     introKey.currentState?.animateScroll(0);
          //   },
          //   child: const Text(
          //     'FooButton',
          //     style: TextStyle(color: Colors.white),
          //   ),
          //   color: Colors.lightBlue,
          //   shape: RoundedRectangleBorder(
          //     borderRadius: BorderRadius.circular(8.0),
          //   ),
          // ),
          decoration: pageDecoration,
        ),
        PageViewModel(
          title: "Get Started!",
          bodyWidget: Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: const [
              Text("Yahooo ", style: bodyStyle),
              // Icon(Icons.edit),
              // Text(" to edit a post", style: bodyStyle),
            ],
          ),
          image: _buildImage(
              'https://i.ibb.co/0GhJgbk/3.png'),
          decoration: pageDecoration,
        ),
      ],
      onDone: () => _onIntroEnd(context),
      //onSkip: () => _onIntroEnd(context), // You can override onSkip callback
      showSkipButton: true,
      skipFlex: 0,
      nextFlex: 0,
      skip: const Text('Skip'),
      next: const Icon(Icons.arrow_forward),
      done: const Text('Done', style: TextStyle(fontWeight: FontWeight.w600)),
      dotsDecorator: const DotsDecorator(
        size: Size(10.0, 10.0),
        color: Color(0xFFBDBDBD),
        activeSize: Size(22.0, 10.0),
        activeShape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0)),
        ),
      ),
    );
  }
}

